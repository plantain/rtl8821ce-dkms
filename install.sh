#!/bin/bash
#
# Setting dependencies for
# build debian packages
#

if [[ "$(id -u)" != "0" ]]; then
    echo "The script must be run as root"
    exit 1
fi

apt-get install \
 autoconf \
 automake \
 libtool \
 autotools-dev \
 dpkg-dev \
 fakeroot \
 devscripts \
 debhelper \
 dkms dh-modaliases
