/* SPDX-License-Identifier: GPL-2.0 */
#include "btc/mp_precomp.h"
#include <hal_btcoex_wifionly.h>

struct  wifi_only_cfg GLBtCoexistWifiOnly;

void halwifionly_write1byte(PVOID pwifionlyContext, u32 RegAddr, u8 Data)
{
	struct wifi_only_cfg *pwifionlycfg = (struct wifi_only_cfg *)pwifionlyContext;
	PADAPTER		Adapter = pwifionlycfg->Adapter;

	rtw_write8(Adapter, RegAddr, Data);
}

void halwifionly_phy_set_bb_reg(PVOID pwifionlyContext, u32 RegAddr, u32 BitMask, u32 Data)
{
	struct wifi_only_cfg *pwifionlycfg = (struct wifi_only_cfg *)pwifionlyContext;
	PADAPTER		Adapter = pwifionlycfg->Adapter;

	phy_set_bb_reg(Adapter, RegAddr, BitMask, Data);
}

void hal_btcoex_wifionly_switchband_notify(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);
	u8 is_5g = _FALSE;

	if (pHalData->current_band_type == BAND_ON_5G)
		is_5g = _TRUE;

	ex_hal8821c_wifi_only_switchbandnotify(&GLBtCoexistWifiOnly, is_5g);
}

void hal_btcoex_wifionly_scan_notify(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);
	u8 is_5g = _FALSE;

	if (pHalData->current_band_type == BAND_ON_5G)
		is_5g = _TRUE;

	ex_hal8821c_wifi_only_scannotify(&GLBtCoexistWifiOnly, is_5g);
}

void hal_btcoex_wifionly_hw_config(PADAPTER padapter)
{
	struct wifi_only_cfg *pwifionlycfg = &GLBtCoexistWifiOnly;

	ex_hal8821c_wifi_only_hw_config(pwifionlycfg);
}

void hal_btcoex_wifionly_initlizevariables(PADAPTER padapter)
{
	struct wifi_only_cfg		*pwifionlycfg = &GLBtCoexistWifiOnly;
	struct wifi_only_haldata	*pwifionly_haldata = &pwifionlycfg->haldata_info;
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	_rtw_memset(&GLBtCoexistWifiOnly, 0, sizeof(GLBtCoexistWifiOnly));

	pwifionlycfg->Adapter = padapter;

	pwifionlycfg->chip_interface = WIFIONLY_INTF_PCI;

	pwifionly_haldata->customer_id = CUSTOMER_NORMAL;
	pwifionly_haldata->efuse_pg_antnum = pHalData->EEPROMBluetoothAntNum;
	pwifionly_haldata->efuse_pg_antpath = pHalData->ant_path;
	pwifionly_haldata->rfe_type = pHalData->rfe_type;
	pwifionly_haldata->ant_div_cfg = pHalData->AntDivCfg;
}
