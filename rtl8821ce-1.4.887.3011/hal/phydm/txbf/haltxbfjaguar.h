/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __HAL_TXBF_JAGUAR_H__
#define __HAL_TXBF_JAGUAR_H__

#define hal_txbf_8812a_set_ndpa_rate(p_dm_void,	BW,	rate)
#define hal_txbf_jaguar_enter(p_dm_void, idx)
#define hal_txbf_jaguar_leave(p_dm_void, idx)
#define hal_txbf_jaguar_status(p_dm_void, idx)
#define hal_txbf_jaguar_fw_txbf(p_dm_void,	idx)
#define hal_txbf_jaguar_patch(p_dm_void, operation)
#define hal_txbf_jaguar_clk_8812a(p_dm_void)

#endif	/*  #ifndef __HAL_TXBF_JAGUAR_H__ */
