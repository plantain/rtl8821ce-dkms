/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/

/* ************************************************************
 * include files
 * ************************************************************ */
#include "mp_precomp.h"
#include "phydm_precomp.h"

void odm_dynamic_bb_power_saving_init(void *p_dm_void)
{
	struct PHY_DM_STRUCT		*p_dm_odm = (struct PHY_DM_STRUCT *)p_dm_void;
	struct _dynamic_power_saving	*p_dm_ps_table = &p_dm_odm->dm_ps_table;

	p_dm_ps_table->pre_cca_state = CCA_MAX;
	p_dm_ps_table->cur_cca_state = CCA_MAX;
	p_dm_ps_table->pre_rf_state = RF_MAX;
	p_dm_ps_table->cur_rf_state = RF_MAX;
	p_dm_ps_table->rssi_val_min = 0;
	p_dm_ps_table->initialize = 0;
}
