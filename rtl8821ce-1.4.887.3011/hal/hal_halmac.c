/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2015 - 2016 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
#define _HAL_HALMAC_C_

#include <drv_types.h>		/* PADAPTER, struct dvobj_priv, SDIO_ERR_VAL8 and etc. */
#include <hal_data.h>		/* efuse, PHAL_DATA_TYPE and etc. */
#include "halmac/halmac_api.h"	/* HALMAC_FW_SIZE_MAX_88XX and etc. */
#include "hal_halmac.h"		/* dvobj_to_halmac() and ect. */

#define DEFAULT_INDICATOR_TIMELMT	1000	/* ms */
#define FIRMWARE_MAX_SIZE		HALMAC_FW_SIZE_MAX_88XX
#define MSG_PREFIX			"[HALMAC]"

/*
 * Driver API for HALMAC operations
 */

static u8 _halmac_reg_read_8(void *p, u32 offset)
{
	struct dvobj_priv *d;
	PADAPTER adapter;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	return rtw_read8(adapter, offset);
}

static u16 _halmac_reg_read_16(void *p, u32 offset)
{
	struct dvobj_priv *d;
	PADAPTER adapter;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	return rtw_read16(adapter, offset);
}

static u32 _halmac_reg_read_32(void *p, u32 offset)
{
	struct dvobj_priv *d;
	PADAPTER adapter;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	return rtw_read32(adapter, offset);
}

static void _halmac_reg_write_8(void *p, u32 offset, u8 val)
{
	struct dvobj_priv *d;
	PADAPTER adapter;
	int err;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	err = rtw_write8(adapter, offset, val);
	if (err == _FAIL)
		RTW_ERR("%s: I/O FAIL!\n", __FUNCTION__);
}

static void _halmac_reg_write_16(void *p, u32 offset, u16 val)
{
	struct dvobj_priv *d;
	PADAPTER adapter;
	int err;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	err = rtw_write16(adapter, offset, val);
	if (err == _FAIL)
		RTW_ERR("%s: I/O FAIL!\n", __FUNCTION__);
}

static void _halmac_reg_write_32(void *p, u32 offset, u32 val)
{
	struct dvobj_priv *d;
	PADAPTER adapter;
	int err;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);

	err = rtw_write32(adapter, offset, val);
	if (err == _FAIL)
		RTW_ERR("%s: I/O FAIL!\n", __FUNCTION__);
}

static u8 _halmac_mfree(void *p, void *buffer, u32 size)
{
	rtw_mfree(buffer, size);
	return _TRUE;
}

static void *_halmac_malloc(void *p, u32 size)
{
	return rtw_zmalloc(size);
}

static u8 _halmac_memcpy(void *p, void *dest, void *src, u32 size)
{
	_rtw_memcpy(dest, src, size);
	return _TRUE;
}

static u8 _halmac_memset(void *p, void *addr, u8 value, u32 size)
{
	_rtw_memset(addr, value, size);
	return _TRUE;
}

static void _halmac_udelay(void *p, u32 us)
{
	rtw_udelay_os(us);
}

static u8 _halmac_mutex_init(void *p, HALMAC_MUTEX *pMutex)
{
	_rtw_mutex_init(pMutex);
	return _TRUE;
}

static u8 _halmac_mutex_deinit(void *p, HALMAC_MUTEX *pMutex)
{
	_rtw_mutex_free(pMutex);
	return _TRUE;
}

static u8 _halmac_mutex_lock(void *p, HALMAC_MUTEX *pMutex)
{
	int err;

	err = _enter_critical_mutex(pMutex, NULL);
	if (err)
		return _FALSE;

	return _TRUE;
}

static u8 _halmac_mutex_unlock(void *p, HALMAC_MUTEX *pMutex)
{
	_exit_critical_mutex(pMutex, NULL);
	return _TRUE;
}

static u8 _halmac_msg_print(void *p, u32 msg_type, u8 msg_level, s8 *fmt, ...)
{
#define MSG_LEN		100
	va_list args;
	u8 str[MSG_LEN] = {0};
	int err;
	u8 ret = _TRUE;

	str[0] = '\n';
	va_start(args, fmt);
	err = vsnprintf(str, MSG_LEN, fmt, args);
	va_end(args);

	/* An output error is encountered */
	if (err < 0)
		return _FALSE;
	/* Output may be truncated due to size limit */
	if ((err == (MSG_LEN - 1)) && (str[MSG_LEN - 2] != '\n'))
		ret = _FALSE;

	if (msg_level == HALMAC_DBG_ALWAYS)
		RTW_PRINT(MSG_PREFIX "%s", str);
	else if (msg_level <= HALMAC_DBG_ERR)
		RTW_ERR(MSG_PREFIX "%s", str);
	else if (msg_level <= HALMAC_DBG_WARN)
		RTW_WARN(MSG_PREFIX "%s", str);
	else
		RTW_DBG(MSG_PREFIX "%s", str);

	return ret;
}

static u8 _halmac_buff_print(void *p, u32 msg_type, u8 msg_level, s8 *buf, u32 size)
{
	if (msg_level <= HALMAC_DBG_WARN)
		RTW_INFO_DUMP(MSG_PREFIX, buf, size);
	else
		RTW_DBG_DUMP(MSG_PREFIX, buf, size);

	return _TRUE;
}

const char *const RTW_HALMAC_FEATURE_NAME[] = {
	"HALMAC_FEATURE_CFG_PARA",
	"HALMAC_FEATURE_DUMP_PHYSICAL_EFUSE",
	"HALMAC_FEATURE_DUMP_LOGICAL_EFUSE",
	"HALMAC_FEATURE_UPDATE_PACKET",
	"HALMAC_FEATURE_UPDATE_DATAPACK",
	"HALMAC_FEATURE_RUN_DATAPACK",
	"HALMAC_FEATURE_CHANNEL_SWITCH",
	"HALMAC_FEATURE_IQK",
	"HALMAC_FEATURE_POWER_TRACKING",
	"HALMAC_FEATURE_PSD",
	"HALMAC_FEATURE_ALL"
};

static inline u8 is_valid_id_status(HALMAC_FEATURE_ID id, HALMAC_CMD_PROCESS_STATUS status)
{
	switch (id) {
	case HALMAC_FEATURE_CFG_PARA:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_DUMP_PHYSICAL_EFUSE:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		if (HALMAC_CMD_PROCESS_DONE != status)
			RTW_INFO("%s: id(%d) unspecified status(%d)!\n",
				 __FUNCTION__, id, status);
		break;
	case HALMAC_FEATURE_DUMP_LOGICAL_EFUSE:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		if (HALMAC_CMD_PROCESS_DONE != status)
			RTW_INFO("%s: id(%d) unspecified status(%d)!\n",
				 __FUNCTION__, id, status);
		break;
	case HALMAC_FEATURE_UPDATE_PACKET:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_UPDATE_DATAPACK:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_RUN_DATAPACK:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_CHANNEL_SWITCH:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_IQK:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_POWER_TRACKING:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_PSD:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	case HALMAC_FEATURE_ALL:
		RTW_INFO("%s: %s\n", __FUNCTION__, RTW_HALMAC_FEATURE_NAME[id]);
		break;
	default:
		RTW_ERR("%s: unknown feature id(%d)\n", __FUNCTION__, id);
		return _FALSE;
	}

	return _TRUE;
}

static int init_halmac_event_with_waittime(struct dvobj_priv *d, HALMAC_FEATURE_ID id, u8 *buf, u32 size, u32 time)
{
	struct submit_ctx *sctx;

	if (!d->hmpriv.indicator[id].sctx) {
		sctx = (struct submit_ctx *)rtw_zmalloc(sizeof(*sctx));
		if (!sctx)
			return -1;
	} else {
		RTW_WARN("%s: id(%d) sctx is not NULL!!\n", __FUNCTION__, id);
		sctx = d->hmpriv.indicator[id].sctx;
		d->hmpriv.indicator[id].sctx = NULL;
	}

	rtw_sctx_init(sctx, time);
	d->hmpriv.indicator[id].buffer = buf;
	d->hmpriv.indicator[id].buf_size = size;
	d->hmpriv.indicator[id].ret_size = 0;
	d->hmpriv.indicator[id].status = 0;
	/* fill sctx at least to sure other variables are all ready! */
	d->hmpriv.indicator[id].sctx = sctx;

	return 0;
}

static inline int init_halmac_event(struct dvobj_priv *d, HALMAC_FEATURE_ID id, u8 *buf, u32 size)
{
	return init_halmac_event_with_waittime(d, id, buf, size, DEFAULT_INDICATOR_TIMELMT);
}

static void free_halmac_event(struct dvobj_priv *d, HALMAC_FEATURE_ID id)
{
	struct submit_ctx *sctx;

	if (!d->hmpriv.indicator[id].sctx)
		return;

	sctx = d->hmpriv.indicator[id].sctx;
	d->hmpriv.indicator[id].sctx = NULL;
	rtw_mfree((u8 *)sctx, sizeof(*sctx));
}

static int wait_halmac_event(struct dvobj_priv *d, HALMAC_FEATURE_ID id)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	struct submit_ctx *sctx;
	int ret;

	sctx = d->hmpriv.indicator[id].sctx;
	if (!sctx)
		return -1;

	ret = rtw_sctx_wait(sctx, RTW_HALMAC_FEATURE_NAME[id]);
	free_halmac_event(d, id);
	if (_SUCCESS == ret)
		return 0;

	/* timeout! We have to reset halmac state */
	RTW_ERR("%s: Wait id(%d, %s) TIMEOUT! Reset HALMAC state!\n",
		__FUNCTION__, id, RTW_HALMAC_FEATURE_NAME[id]);
	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);
	api->halmac_reset_feature(mac, id);

	return -1;
}

/*
 * Return:
 *	Always return _TRUE, HALMAC don't care the return value.
 */
static u8 _halmac_event_indication(void *p, HALMAC_FEATURE_ID feature_id, HALMAC_CMD_PROCESS_STATUS process_status, u8 *buf, u32 size)
{
	struct dvobj_priv *d;
	PADAPTER adapter;
	PHAL_DATA_TYPE hal;
	struct halmac_indicator *tbl, *indicator;
	struct submit_ctx *sctx;
	u32 cpsz;
	u8 ret;

	d = (struct dvobj_priv *)p;
	adapter = dvobj_get_primary_adapter(d);
	hal = GET_HAL_DATA(adapter);
	tbl = d->hmpriv.indicator;

	/* Filter(Skip) middle status indication */
	ret = is_valid_id_status(feature_id, process_status);
	if (_FALSE == ret)
		goto exit;

	indicator = &tbl[feature_id];
	indicator->status = process_status;
	indicator->ret_size = size;
	if (!indicator->sctx) {
		RTW_WARN("%s: No feature id(%d, %s) waiting!!\n", __FUNCTION__, feature_id, RTW_HALMAC_FEATURE_NAME[feature_id]);
		goto exit;
	}
	sctx = indicator->sctx;

	if (HALMAC_CMD_PROCESS_ERROR == process_status) {
		RTW_ERR("%s: Something wrong id(%d, %s)!!\n", __FUNCTION__, feature_id, RTW_HALMAC_FEATURE_NAME[feature_id]);
		rtw_sctx_done_err(&sctx, RTW_SCTX_DONE_UNKNOWN);
		goto exit;
	}

	if (size > indicator->buf_size) {
		RTW_WARN("%s: id(%d, %s) buffer is not enough(%d<%d), data will be truncated!\n",
			 __FUNCTION__, feature_id, RTW_HALMAC_FEATURE_NAME[feature_id], indicator->buf_size, size);
		cpsz = indicator->buf_size;
	} else {
		cpsz = size;
	}
	if (cpsz && indicator->buffer)
		_rtw_memcpy(indicator->buffer, buf, cpsz);

	rtw_sctx_done(&sctx);

exit:
	return _TRUE;
}

HALMAC_PLATFORM_API rtw_halmac_platform_api = {
	/* R/W register */
	.REG_READ_8 = _halmac_reg_read_8,
	.REG_READ_16 = _halmac_reg_read_16,
	.REG_READ_32 = _halmac_reg_read_32,
	.REG_WRITE_8 = _halmac_reg_write_8,
	.REG_WRITE_16 = _halmac_reg_write_16,
	.REG_WRITE_32 = _halmac_reg_write_32,

	/* Write data */
	/* Memory allocate */
	.RTL_FREE = _halmac_mfree,
	.RTL_MALLOC = _halmac_malloc,
	.RTL_MEMCPY = _halmac_memcpy,
	.RTL_MEMSET = _halmac_memset,

	/* Sleep */
	.RTL_DELAY_US = _halmac_udelay,

	/* Process Synchronization */
	.MUTEX_INIT = _halmac_mutex_init,
	.MUTEX_DEINIT = _halmac_mutex_deinit,
	.MUTEX_LOCK = _halmac_mutex_lock,
	.MUTEX_UNLOCK = _halmac_mutex_unlock,

	.MSG_PRINT = _halmac_msg_print,
	.BUFF_PRINT = _halmac_buff_print,
	.EVENT_INDICATION = _halmac_event_indication,
};

static int init_priv(struct halmacpriv *priv)
{
	struct halmac_indicator *indicator;
	u32 count, size;

	size = sizeof(*priv);
	_rtw_memset(priv, 0, size);

	count = HALMAC_FEATURE_ALL + 1;
	size = sizeof(*indicator) * count;
	indicator = (struct halmac_indicator *)rtw_zmalloc(size);
	if (!indicator)
		return -1;
	priv->indicator = indicator;

	return 0;
}

static void deinit_priv(struct halmacpriv *priv)
{
	struct halmac_indicator *indicator;

	indicator = priv->indicator;
	priv->indicator = NULL;
	if (indicator) {
		u32 count, size;

		count = HALMAC_FEATURE_ALL + 1;
		{
			struct submit_ctx *sctx;
			u32 i;

			for (i = 0; i < count; i++) {
				if (!indicator[i].sctx)
					continue;

				RTW_WARN("%s: %s id(%d) sctx still exist!!\n",
					__FUNCTION__, RTW_HALMAC_FEATURE_NAME[i], i);
				sctx = indicator[i].sctx;
				indicator[i].sctx = NULL;
				rtw_mfree((u8 *)sctx, sizeof(*sctx));
			}
		}
		size = sizeof(*indicator) * count;
		rtw_mfree((u8 *)indicator, size);
	}
}

int rtw_halmac_init_adapter(struct dvobj_priv *d, PHALMAC_PLATFORM_API pf_api)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_INTERFACE intf;
	HALMAC_RET_STATUS status;
	int err = 0;

	halmac = dvobj_to_halmac(d);
	if (halmac) {
		err = 0;
		goto out;
	}

	err = init_priv(&d->hmpriv);
	if (err)
		goto out;

	intf = HALMAC_INTERFACE_PCIE;
	status = halmac_init_adapter(d, pf_api, intf, &halmac, &api);
	if (HALMAC_RET_SUCCESS != status) {
		RTW_ERR("%s: halmac_init_adapter fail!(status=%d)\n", __FUNCTION__, status);
		err = -1;
		goto out;
	}

	dvobj_set_halmac(d, halmac);

	status = api->halmac_phy_cfg(halmac, HALMAC_INTF_PHY_PLATFORM_ALL);
	if (status != HALMAC_RET_SUCCESS) {
		RTW_ERR("%s: halmac_phy_cfg fail!(status=%d)\n", __FUNCTION__, status);
		err = -1;
		goto out;
	}

out:
	if (err)
		rtw_halmac_deinit_adapter(d);

	return err;
}

int rtw_halmac_deinit_adapter(struct dvobj_priv *d)
{
	PHALMAC_ADAPTER halmac;
	HALMAC_RET_STATUS status;
	int err = 0;

	halmac = dvobj_to_halmac(d);
	if (!halmac) {
		err = 0;
		goto out;
	}

	deinit_priv(&d->hmpriv);

	status = halmac_deinit_adapter(halmac);
	dvobj_set_halmac(d, NULL);
	if (status != HALMAC_RET_SUCCESS) {
		err = -1;
		goto out;
	}

out:
	return err;
}

/*
 * Description:
 *	Power on device hardware.
 *	[Notice!] If device's power state is on before,
 *	it would be power off first and turn on power again.
 *
 * Return:
 *	0	power on success
 *	-1	power on fail
 *	-2	power state unchange
 */
int rtw_halmac_poweron(struct dvobj_priv *d)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = -1;

	halmac = dvobj_to_halmac(d);
	if (!halmac)
		goto out;

	api = HALMAC_GET_API(halmac);

	status = api->halmac_pre_init_system_cfg(halmac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	status = api->halmac_mac_power_switch(halmac, HALMAC_MAC_POWER_ON);
	if (HALMAC_RET_PWR_UNCHANGE == status) {
		/*
		 * Work around for warm reboot but device not power off,
		 * but it would also fall into this case when auto power on is enabled.
		 */
		api->halmac_mac_power_switch(halmac, HALMAC_MAC_POWER_OFF);
		status = api->halmac_mac_power_switch(halmac, HALMAC_MAC_POWER_ON);
		RTW_WARN("%s: Power state abnormal, try to recover...%s\n",
			 __FUNCTION__, (HALMAC_RET_SUCCESS == status)?"OK":"FAIL!");
	}
	if (HALMAC_RET_SUCCESS != status) {
		if (HALMAC_RET_PWR_UNCHANGE == status)
			err = -2;
		goto out;
	}

	status = api->halmac_init_system_cfg(halmac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}

/*
 * Description:
 *	Power off device hardware.
 *
 * Return:
 *	0	Power off success
 *	-1	Power off fail
 */
int rtw_halmac_poweroff(struct dvobj_priv *d)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = -1;

	halmac = dvobj_to_halmac(d);
	if (!halmac)
		goto out;

	api = HALMAC_GET_API(halmac);

	status = api->halmac_mac_power_switch(halmac, HALMAC_MAC_POWER_OFF);
	if ((HALMAC_RET_SUCCESS != status)
	    && (HALMAC_RET_PWR_UNCHANGE != status))
		goto out;

	err = 0;
out:
	return err;
}

/*
 * Note:
 *	When this function return, the register REG_RCR may be changed.
 */
int rtw_halmac_config_rx_info(struct dvobj_priv *d, HALMAC_DRV_INFO info)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = -1;

	halmac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(halmac);

	status = api->halmac_cfg_drv_info(halmac, info);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}


static u8 _get_drv_rsvd_page(HALMAC_DRV_RSVD_PG_NUM rsvd_page_number)
{
	if (HALMAC_RSVD_PG_NUM16 == rsvd_page_number)
		return 16;
	else if (HALMAC_RSVD_PG_NUM24 == rsvd_page_number)
		return 24;
	else if (HALMAC_RSVD_PG_NUM32 == rsvd_page_number)
		return 32;

	RTW_ERR("%s unknown HALMAC_RSVD_PG type :%d\n", __func__, rsvd_page_number);
	rtw_warn_on(1);
	return 0;
}

static HALMAC_TRX_MODE _choose_trx_mode(struct dvobj_priv *d)
{
	PADAPTER p;

	p = dvobj_get_primary_adapter(d);

	if (p->registrypriv.wifi_spec)
		return HALMAC_TRX_MODE_WMM;


	return HALMAC_TRX_MODE_NORMAL;
}

static inline HALMAC_RF_TYPE _rf_type_drv2halmac(RT_RF_TYPE_DEF_E rf_drv)
{
	HALMAC_RF_TYPE rf_mac;

	switch (rf_drv) {
	case RF_1T2R:
		rf_mac = HALMAC_RF_1T2R;
		break;
	case RF_2T4R:
		rf_mac = HALMAC_RF_2T4R;
		break;
	case RF_2T2R:
		rf_mac = HALMAC_RF_2T2R;
		break;
	case RF_1T1R:
		rf_mac = HALMAC_RF_1T1R;
		break;
	case RF_2T2R_GREEN:
		rf_mac = HALMAC_RF_2T2R_GREEN;
		break;
	case RF_2T3R:
		rf_mac = HALMAC_RF_2T3R;
		break;
	case RF_3T3R:
		rf_mac = HALMAC_RF_3T3R;
		break;
	case RF_3T4R:
		rf_mac = HALMAC_RF_3T4R;
		break;
	case RF_4T4R:
		rf_mac = HALMAC_RF_4T4R;
		break;
	default:
		rf_mac = (HALMAC_RF_TYPE)rf_drv;
		break;
	}

	return rf_mac;
}

static int _send_general_info(struct dvobj_priv *d)
{
	PADAPTER adapter;
	PHAL_DATA_TYPE hal;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_GENERAL_INFO info;
	HALMAC_RET_STATUS status;
	u8 val8;

	adapter = dvobj_get_primary_adapter(d);
	hal = GET_HAL_DATA(adapter);
	halmac = dvobj_to_halmac(d);
	if (!halmac)
		return -1;
	api = HALMAC_GET_API(halmac);

	_rtw_memset(&info, 0, sizeof(info));
	info.rfe_type = (u8)hal->rfe_type;
	rtw_hal_get_hwreg(adapter, HW_VAR_RF_TYPE, &val8);
	info.rf_type = _rf_type_drv2halmac(val8);

	status = api->halmac_send_general_info(halmac, &info);
	switch (status) {
	case HALMAC_RET_SUCCESS:
		break;
	case HALMAC_RET_NO_DLFW:
		RTW_WARN("%s: halmac_send_general_info() fail because fw not dl!\n",
			 __FUNCTION__);
		/* go through */
	default:
		return -1;
	}

	return 0;
}

/*
 * Description:
 *	Downlaod Firmware Flow
 *
 * Parameters:
 *	d	pointer of struct dvobj_priv
 *	fw	firmware array
 *	fwsize	firmware size
 *	re_dl	re-download firmware or not
 *		0: run in init hal flow, not re-download
 *		1: it is a stand alone operation, not in init hal flow
 *
 * Return:
 *	0	Success
 *	others	Fail
 */
static int download_fw(struct dvobj_priv *d, u8 *fw, u32 fwsize, u8 re_dl)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = 0;
	PHAL_DATA_TYPE hal;
	HALMAC_FW_VERSION fw_vesion;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);
	hal = GET_HAL_DATA(dvobj_get_primary_adapter(d));

	if ((!fw) || (!fwsize))
		return -1;

	/* 1. Driver Stop Tx */
	/* ToDo */

	/* 2. Driver Check Tx FIFO is empty */
	/* ToDo */

	/* 3. Config MAX download size */
	/* required a even length from u32 */
	api->halmac_cfg_max_dl_size(mac, (MAX_CMDBUF_SZ - TXDESC_OFFSET) & 0xFFFFFFFE);

	/* 4. Download Firmware */
	status = api->halmac_download_firmware(mac, fw, fwsize);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	/* 5. Driver resume TX if needed */
	/* ToDo */

	if (re_dl) {
		HALMAC_TRX_MODE mode;

		/* 6. Init TRX Configuration */
		mode = _choose_trx_mode(d);
		status = api->halmac_init_trx_cfg(mac, mode);
		if (HALMAC_RET_SUCCESS != status)
			return -1;

		/* 7. Config RX Aggregation */
		err = rtw_halmac_rx_agg_switch(d, _TRUE);
		if (err)
			return -1;

		/* 8. Send General Info */
		err = _send_general_info(d);
		if (err)
			return -1;
	}

	/* 9. Reset driver variables if needed */
	hal->LastHMEBoxNum = 0;

	/* 10. Get FW version */
	status = api->halmac_get_fw_version(mac, &fw_vesion);
	if (status == HALMAC_RET_SUCCESS) {
		hal->firmware_version = fw_vesion.version;
		hal->firmware_sub_version = fw_vesion.sub_version;
		hal->firmware_size = fwsize;
	}

	return err;
}

static HALMAC_RET_STATUS init_mac_flow(struct dvobj_priv *d)
{
	PADAPTER p;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_TRX_MODE trx_mode;
	HALMAC_RET_STATUS status;
	u8 nettype;
	int err;
	PHAL_DATA_TYPE hal;
	HALMAC_DRV_RSVD_PG_NUM rsvd_page_number = HALMAC_RSVD_PG_NUM16;/*HALMAC_RSVD_PG_NUM24/HALMAC_RSVD_PG_NUM32*/

	p = dvobj_get_primary_adapter(d);
	hal = GET_HAL_DATA(p);
	halmac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(halmac);


	status = api->halmac_cfg_drv_rsvd_pg_num(halmac, rsvd_page_number);
	if (status != HALMAC_RET_SUCCESS)
		goto out;
	hal->drv_rsvd_page_number = _get_drv_rsvd_page(rsvd_page_number);

	trx_mode = _choose_trx_mode(d);
	status = api->halmac_init_mac_cfg(halmac, trx_mode);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = rtw_halmac_rx_agg_switch(d, _TRUE);
	if (err)
		goto out;

	nettype = dvobj_to_regsty(d)->wireless_mode;
	if (is_supported_vht(nettype) == _TRUE)
		status = api->halmac_cfg_operation_mode(halmac, HALMAC_WIRELESS_MODE_AC);
	else if (is_supported_ht(nettype) == _TRUE)
		status = api->halmac_cfg_operation_mode(halmac, HALMAC_WIRELESS_MODE_N);
	else if (IsSupportedTxOFDM(nettype) == _TRUE)
		status = api->halmac_cfg_operation_mode(halmac, HALMAC_WIRELESS_MODE_G);
	else
		status = api->halmac_cfg_operation_mode(halmac, HALMAC_WIRELESS_MODE_B);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

out:
	return status;
}

/*
 * Notices:
 *	Make sure
 *	1. rtw_hal_get_hwreg(HW_VAR_RF_TYPE)
 *	2. HAL_DATA_TYPE.rfe_type
 *	already ready for use before calling this function.
 */
static int _halmac_init_hal(struct dvobj_priv *d, u8 *fw, u32 fwsize)
{
	PADAPTER adapter;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 ok = _TRUE;
	u8 fw_ok = _FALSE;
	int err, err_ret = -1;

	adapter = dvobj_get_primary_adapter(d);
	halmac = dvobj_to_halmac(d);
	if (!halmac)
		goto out;
	api = HALMAC_GET_API(halmac);

	/* StatePowerOff */

	/* SKIP: halmac_init_adapter (Already done before) */

	/* halmac_pre_Init_system_cfg */
	/* halmac_mac_power_switch(on) */
	/* halmac_Init_system_cfg */
	ok = rtw_hal_power_on(adapter);
	if (_FAIL == ok)
		goto out;

	/* StatePowerOn */

	/* DownloadFW */
	if (fw && fwsize) {
		err = download_fw(d, fw, fwsize, 0);
		if (err)
			goto out;
		fw_ok = _TRUE;
	}

	/* InitMACFlow */
	status = init_mac_flow(d);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	/* halmac_send_general_info */
	if (_TRUE == fw_ok) {
		err = _send_general_info(d);
		if (err)
			goto out;
	}

	/* Init Phy parameter-MAC */
	ok = rtw_hal_init_mac_register(adapter);
	if (_FALSE == ok)
		goto out;

	/* StateMacInitialized */

	/* halmac_cfg_drv_info */
	err = rtw_halmac_config_rx_info(d, HALMAC_DRV_INFO_PHY_STATUS);
	if (err)
		goto out;

	/* halmac_set_hw_value(HALMAC_HW_EN_BB_RF) */
	/* Init BB, RF */
	ok = rtw_hal_init_phy(adapter);
	if (_FALSE == ok)
		goto out;

	status = api->halmac_init_interface_cfg(halmac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	/* SKIP: halmac_verify_platform_api */
	/* SKIP: halmac_h2c_lb */

	/* StateRxIdle */

	err_ret = 0;
out:
	return err_ret;
}

/*
 * Notices:
 *	Make sure
 *	1. rtw_hal_get_hwreg(HW_VAR_RF_TYPE)
 *	2. HAL_DATA_TYPE.rfe_type
 *	already ready for use before calling this function.
 */
int rtw_halmac_init_hal_fw(struct dvobj_priv *d, u8 *fw, u32 fwsize)
{
	return _halmac_init_hal(d, fw, fwsize);
}

int rtw_halmac_deinit_hal(struct dvobj_priv *d)
{
	PADAPTER adapter;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = -1;

	adapter = dvobj_get_primary_adapter(d);
	halmac = dvobj_to_halmac(d);
	if (!halmac)
		goto out;
	api = HALMAC_GET_API(halmac);

	status = api->halmac_deinit_interface_cfg(halmac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	rtw_hal_power_off(adapter);

	err = 0;
out:
	return err;
}

int rtw_halmac_self_verify(struct dvobj_priv *d)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = -1;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_verify_platform_api(mac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	status = api->halmac_h2c_lb(mac);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}

u8 rtw_halmac_txfifo_is_empty(struct dvobj_priv *d)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	u8 rst = _TRUE;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);
	if (HALMAC_RET_TXFIFO_NO_EMPTY == api->halmac_txfifo_is_empty(mac, 10))
		rst = _FALSE;

	return rst;
}

static HALMAC_DLFW_MEM _get_halmac_fw_mem(enum fw_mem mem)
{
	if (FW_EMEM == mem)
		return HALMAC_DLFW_MEM_EMEM;
	else if (FW_IMEM == mem)
		return HALMAC_DLFW_MEM_UNDEFINE;
	else if (FW_DMEM == mem)
		return HALMAC_DLFW_MEM_UNDEFINE;
	else
		return HALMAC_DLFW_MEM_UNDEFINE;
}

#define DBG_DL_FW_MEM
int rtw_halmac_dlfw_mem(struct dvobj_priv *d, u8 *fw, u32 fwsize, enum fw_mem mem)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int err = 0;
	u8 chk_cnt = 0;
	bool txfifo_empty = _FALSE;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	if ((!fw) || (!fwsize))
		return -1;

	/* 1. Driver Stop Tx */
	/* ToDo */

	/* 2. Driver Check Tx FIFO is empty */
	do {
		txfifo_empty = rtw_halmac_txfifo_is_empty(d);
		chk_cnt++;
		RTW_INFO("polling txfifo empty chk_cnt:%d\n", chk_cnt);
		rtw_msleep_os(2);
	} while ((!txfifo_empty) && (chk_cnt < 100));

	if (_FALSE == txfifo_empty) {
		{
			PADAPTER adapter = dvobj_get_primary_adapter(d);

			RTW_ERR("%s => polling txfifo empty failed\n", __func__);
			RTW_ERR("REG_210:0x%08x\n", rtw_read32(adapter, 0x210));
			RTW_ERR("REG_230:0x%08x\n", rtw_read32(adapter, 0x230));
			RTW_ERR("REG_234:0x%08x\n", rtw_read32(adapter, 0x234));
			RTW_ERR("REG_238:0x%08x\n", rtw_read32(adapter, 0x238));
			RTW_ERR("REG_23C:0x%08x\n", rtw_read32(adapter, 0x23C));
			RTW_ERR("REG_240:0x%08x\n", rtw_read32(adapter, 0x240));
			RTW_ERR("REG_41A:0x%08x\n", rtw_read32(adapter, 0x41A));

			RTW_ERR("REG_10FC:0x%08x\n", rtw_read32(adapter, 0x10FC));
			RTW_ERR("REG_10F8:0x%08x\n", rtw_read32(adapter, 0x10F8));
			RTW_ERR("REG_11F4:0x%08x\n", rtw_read32(adapter, 0x11F4));
			RTW_ERR("REG_11F8:0x%08x\n", rtw_read32(adapter, 0x11F8));
		}
		return -1;
	}

	/* 3. Download Firmware MEM */
	status = api->halmac_free_download_firmware(mac, _get_halmac_fw_mem(mem), fw, fwsize);
	if (HALMAC_RET_SUCCESS != status) {
		RTW_ERR("%s => halmac_free_download_firmware failed\n", __func__);
		return -1;
	}
	/* 4. Driver resume TX if needed */
	/* ToDo */

	return err;
}

/*
 * Return:
 *	0	Success
 *	-22	Invalid arguemnt
 */
int rtw_halmac_dlfw(struct dvobj_priv *d, u8 *fw, u32 fwsize)
{
	PADAPTER adapter;
	HALMAC_RET_STATUS status;
	u32 ok = _TRUE;
	int err, err_ret = -1;

	if (!fw || !fwsize)
		return -22;

	adapter = dvobj_get_primary_adapter(d);

	/* re-download firmware */
	if (rtw_is_hw_init_completed(adapter))
		return download_fw(d, fw, fwsize, 1);

	/* Download firmware before hal init */
	/* Power on, download firmware and init mac */
	ok = rtw_hal_power_on(adapter);
	if (_FAIL == ok)
		goto out;

	err = download_fw(d, fw, fwsize, 0);
	if (err) {
		err_ret = err;
		goto out;
	}

	status = init_mac_flow(d);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = _send_general_info(d);
	if (err)
		goto out;

	err_ret = 0;

out:
	return err_ret;
}

static u8 _is_fw_read_cmd_down(PADAPTER adapter, u8 msgbox_num)
{
	u8 read_down = _FALSE;
	int retry_cnts = 100;
	u8 valid;

	do {
		valid = rtw_read8(adapter, REG_HMETFR) & BIT(msgbox_num);
		if (0 == valid)
			read_down = _TRUE;
		else
			rtw_msleep_os(1);
	} while ((!read_down) && (retry_cnts--));

	if (_FALSE == read_down)
		RTW_WARN("%s, reg_1cc(%x), msg_box(%d)...\n", __func__, rtw_read8(adapter, REG_HMETFR), msgbox_num);

	return read_down;
}

int rtw_halmac_send_h2c(struct dvobj_priv *d, u8 *h2c)
{
	PADAPTER adapter = dvobj_get_primary_adapter(d);
	PHAL_DATA_TYPE hal = GET_HAL_DATA(adapter);
	u8 h2c_box_num = 0;
	u32 msgbox_addr = 0;
	u32 msgbox_ex_addr = 0;
	u32 h2c_cmd = 0;
	u32 h2c_cmd_ex = 0;
	s32 ret = _FAIL;

	if (adapter->bFWReady == _FALSE) {
		RTW_WARN("%s: return H2C cmd because fw is not ready\n", __FUNCTION__);
		return ret;
	}

	if (!h2c) {
		RTW_WARN("%s: pbuf is NULL\n", __FUNCTION__);
		return ret;
	}

	if (rtw_is_surprise_removed(adapter)) {
		RTW_WARN("%s: surprise removed\n", __FUNCTION__);
		return ret;
	}

	_enter_critical_mutex(&d->h2c_fwcmd_mutex, NULL);

	/* pay attention to if race condition happened in  H2C cmd setting */
	h2c_box_num = hal->LastHMEBoxNum;

	if (!_is_fw_read_cmd_down(adapter, h2c_box_num)) {
		RTW_WARN(" fw read cmd failed...\n");
		goto exit;
	}

	/* Write Ext command(byte 4 -7) */
	msgbox_ex_addr = REG_HMEBOX_E0 + (h2c_box_num * EX_MESSAGE_BOX_SIZE);
	_rtw_memcpy((u8 *)(&h2c_cmd_ex), h2c + 4, EX_MESSAGE_BOX_SIZE);
	h2c_cmd_ex = le32_to_cpu(h2c_cmd_ex);
	rtw_write32(adapter, msgbox_ex_addr, h2c_cmd_ex);

	/* Write command (byte 0 -3 ) */
	msgbox_addr = REG_HMEBOX0 + (h2c_box_num * MESSAGE_BOX_SIZE);
	_rtw_memcpy((u8 *)(&h2c_cmd), h2c, 4);
	h2c_cmd = le32_to_cpu(h2c_cmd);
	rtw_write32(adapter, msgbox_addr, h2c_cmd);

	/* update last msg box number */
	hal->LastHMEBoxNum = (h2c_box_num + 1) % MAX_H2C_BOX_NUMS;
	ret = _SUCCESS;

exit:
	_exit_critical_mutex(&d->h2c_fwcmd_mutex, NULL);
	return ret;
}

int rtw_halmac_c2h_handle(struct dvobj_priv *d, u8 *c2h, u32 size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_get_c2h_info(mac, c2h, size);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	return 0;
}

int rtw_halmac_get_available_efuse_size(struct dvobj_priv *d, u32 *size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 val;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_get_efuse_available_size(mac, &val);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	*size = val;
	return 0;
}

int rtw_halmac_get_physical_efuse_size(struct dvobj_priv *d, u32 *size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 val;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_get_efuse_size(mac, &val);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	*size = val;
	return 0;
}

int rtw_halmac_read_physical_efuse_map(struct dvobj_priv *d, u8 *map, u32 size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	HALMAC_FEATURE_ID id;
	int ret;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);
	id = HALMAC_FEATURE_DUMP_PHYSICAL_EFUSE;

	ret = init_halmac_event(d, id, map, size);
	if (ret)
		return -1;

	status = api->halmac_dump_efuse_map(mac, HALMAC_EFUSE_R_AUTO);
	if (HALMAC_RET_SUCCESS != status) {
		free_halmac_event(d, id);
		return -1;
	}

	ret = wait_halmac_event(d, id);
	if (ret)
		return -1;

	return 0;
}

int rtw_halmac_read_physical_efuse(struct dvobj_priv *d, u32 offset, u32 cnt, u8 *data)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u8 v;
	u32 i;
	u8 *efuse = NULL;
	u32 size = 0;
	int err = 0;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	if (api->halmac_read_efuse) {
		for (i = 0; i < cnt; i++) {
			status = api->halmac_read_efuse(mac, offset + i, &v);
			if (HALMAC_RET_SUCCESS != status)
				return -1;
			data[i] = v;
		}
	} else {
		err = rtw_halmac_get_physical_efuse_size(d, &size);
		if (err)
			return -1;

		efuse = rtw_zmalloc(size);
		if (!efuse)
			return -1;

		err = rtw_halmac_read_physical_efuse_map(d, efuse, size);
		if (err)
			err = -1;
		else
			_rtw_memcpy(data, efuse + offset, cnt);

		rtw_mfree(efuse, size);
	}

	return err;
}

int rtw_halmac_write_physical_efuse(struct dvobj_priv *d, u32 offset, u32 cnt, u8 *data)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 i;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	if (api->halmac_write_efuse == NULL)
		return -1;

	for (i = 0; i < cnt; i++) {
		status = api->halmac_write_efuse(mac, offset + i, data[i]);
		if (HALMAC_RET_SUCCESS != status)
			return -1;
	}

	return 0;
}

int rtw_halmac_get_logical_efuse_size(struct dvobj_priv *d, u32 *size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 val;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_get_logical_efuse_size(mac, &val);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	*size = val;
	return 0;
}

int rtw_halmac_read_logical_efuse_map(struct dvobj_priv *d, u8 *map, u32 size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	HALMAC_FEATURE_ID id;
	int ret;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);
	id = HALMAC_FEATURE_DUMP_LOGICAL_EFUSE;

	ret = init_halmac_event(d, id, map, size);
	if (ret)
		return -1;

	status = api->halmac_dump_logical_efuse_map(mac, HALMAC_EFUSE_R_DRV);
	if (HALMAC_RET_SUCCESS != status) {
		free_halmac_event(d, id);
		return -1;
	}

	ret = wait_halmac_event(d, id);
	if (ret)
		return -1;

	return 0;
}

int rtw_halmac_write_logical_efuse_map(struct dvobj_priv *d, u8 *map, u32 size, u8 *maskmap, u32 masksize)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_PG_EFUSE_INFO pginfo;
	HALMAC_RET_STATUS status;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	pginfo.pEfuse_map = map;
	pginfo.efuse_map_size = size;
	pginfo.pEfuse_mask = maskmap;
	pginfo.efuse_mask_size = masksize;

	status = api->halmac_pg_efuse_by_map(mac, &pginfo, HALMAC_EFUSE_R_AUTO);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	return 0;
}

int rtw_halmac_read_logical_efuse(struct dvobj_priv *d, u32 offset, u32 cnt, u8 *data)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u8 v;
	u32 i;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	for (i = 0; i < cnt; i++) {
		status = api->halmac_read_logical_efuse(mac, offset + i, &v);
		if (HALMAC_RET_SUCCESS != status)
			return -1;
		data[i] = v;
	}

	return 0;
}

int rtw_halmac_write_bt_physical_efuse(struct dvobj_priv *d, u32 offset, u32 cnt, u8 *data)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u32 i;
	u8 bank = 1;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	for (i = 0; i < cnt; i++) {
		status = api->halmac_write_efuse_bt(mac, offset + i, data[i], bank);
		if (HALMAC_RET_SUCCESS != status) {
			printk("%s: halmac_write_efuse_bt status = %d\n", __FUNCTION__, status);
			return -1;
		}
	}
	printk("%s: halmac_write_efuse_bt status = HALMAC_RET_SUCCESS %d\n", __FUNCTION__, status);
	return 0;
}

int rtw_halmac_read_bt_physical_efuse_map(struct dvobj_priv *d, u8 *map, u32 size)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	int bank = 1;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_dump_efuse_map_bt(mac, bank, size, map);
	if (HALMAC_RET_SUCCESS != status) {
		printk("%s: halmac_dump_efuse_map_bt fail!\n", __FUNCTION__);
		return -1;
	}

	printk("%s: OK!\n", __FUNCTION__);

	return 0;
}

static inline u8 _hw_port_drv2halmac(enum _hw_port hwport)
{
	u8 port = 0;

	switch (hwport) {
	case HW_PORT0:
		port = 0;
		break;
	case HW_PORT1:
		port = 1;
		break;
	case HW_PORT2:
		port = 2;
		break;
	case HW_PORT3:
		port = 3;
		break;
	case HW_PORT4:
		port = 4;
		break;
	default:
		port = hwport;
		break;
	}

	return port;
}

int rtw_halmac_set_mac_address(struct dvobj_priv *d, enum _hw_port hwport, u8 *addr)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	u8 port;
	HALMAC_WLAN_ADDR hwa;
	HALMAC_RET_STATUS status;
	int err = -1;

	halmac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(halmac);

	port = _hw_port_drv2halmac(hwport);
	_rtw_memset(&hwa, 0, sizeof(hwa));
	_rtw_memcpy(hwa.Address, addr, 6);

	status = api->halmac_cfg_mac_addr(halmac, port, &hwa);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}

int rtw_halmac_set_bssid(struct dvobj_priv *d, enum _hw_port hwport, u8 *addr)
{
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	u8 port;
	HALMAC_WLAN_ADDR hwa;
	HALMAC_RET_STATUS status;
	int err = -1;

	halmac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(halmac);
	port = _hw_port_drv2halmac(hwport);

	_rtw_memset(&hwa, 0, sizeof(HALMAC_WLAN_ADDR));
	_rtw_memcpy(hwa.Address, addr, 6);
	status = api->halmac_cfg_bssid(halmac, port, &hwa);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}

int rtw_halmac_set_bandwidth(struct dvobj_priv *d, u8 channel, u8 pri_ch_idx, u8 bw)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	status = api->halmac_cfg_ch_bw(mac, channel, pri_ch_idx, bw);
	if (HALMAC_RET_SUCCESS != status)
		return -1;

	return 0;
}

static HAL_FIFO_SEL _fifo_sel_drv2halmac(u8 fifo_sel)
{
	if (0 == fifo_sel)
		return HAL_FIFO_SEL_TX;
	else if (1 == fifo_sel)
		return HAL_FIFO_SEL_RX;
	else if (2 == fifo_sel)
		return HAL_FIFO_SEL_RSVD_PAGE;
	else if (3 == fifo_sel)
		return HAL_FIFO_SEL_REPORT;
	else if (4 == fifo_sel)
		return HAL_FIFO_SEL_LLT;
	else
		return HAL_FIFO_SEL_RSVD_PAGE;
}

#define CONFIG_HALMAC_FIFO_DUMP
int rtw_halmac_dump_fifo(struct dvobj_priv *d, u8 fifo_sel, u32 addr, u32 size, u8 *buffer)
{
	PHALMAC_ADAPTER mac;
	PHALMAC_API api;
	HALMAC_RET_STATUS status;
	u8 *pfifo_map = NULL;
	u32 fifo_size = 0;
	s8 ret = 0;/* 0:success, -1:error */
	u8 mem_created = _FALSE;

	HAL_FIFO_SEL halmac_fifo_sel;

	mac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(mac);

	if ((size != 0) && (buffer == NULL))
		return -1;

	halmac_fifo_sel = _fifo_sel_drv2halmac(fifo_sel);

	if ((size) && (buffer)) {
		pfifo_map = buffer;
		fifo_size = size;
	} else {
		fifo_size = api->halmac_get_fifo_size(mac, halmac_fifo_sel);

		if (fifo_size)
			pfifo_map = rtw_zvmalloc(fifo_size);
		if (pfifo_map == NULL)
			return -1;
		mem_created = _TRUE;
	}

	status = api->halmac_dump_fifo(mac, halmac_fifo_sel, addr, fifo_size, pfifo_map);
	if (HALMAC_RET_SUCCESS != status) {
		ret = -1;
		goto _exit;
	}

	{
		static const char * const fifo_sel_str[] = {
			"TX", "RX", "RSVD_PAGE", "REPORT", "LLT"
		};

		RTW_INFO("%s FIFO DUMP [start_addr:0x%04x , size:%d]\n", fifo_sel_str[halmac_fifo_sel], addr, fifo_size);
		RTW_INFO_DUMP("\n", pfifo_map, fifo_size);
		RTW_INFO(" ==================================================\n");
	}

_exit:
	if (mem_created && pfifo_map)
		rtw_vmfree(pfifo_map, fifo_size);
	return ret;

}

int rtw_halmac_rx_agg_switch(struct dvobj_priv *d, u8 enable)
{
	PADAPTER adapter;
	PHAL_DATA_TYPE hal;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	HALMAC_RXAGG_CFG rxaggcfg;
	HALMAC_RET_STATUS status;
	int err = -1;

	adapter = dvobj_get_primary_adapter(d);
	hal = GET_HAL_DATA(adapter);
	halmac = dvobj_to_halmac(d);
	api = HALMAC_GET_API(halmac);
	_rtw_memset((void *)&rxaggcfg, 0, sizeof(rxaggcfg));

	if (_TRUE == enable) {
	} else
		rxaggcfg.mode = HALMAC_RX_AGG_MODE_NONE;

	status = api->halmac_cfg_rx_aggregation(halmac, &rxaggcfg);
	if (status != HALMAC_RET_SUCCESS)
		goto out;

	err = 0;
out:
	return err;
}

/*
 * Description:
 *	Get RX driver info size. RX driver info is a small memory space between
 *	scriptor and RX payload.
 *
 *	+-------------------------+
 *	| RX descriptor           |
 *	| usually 24 bytes        |
 *	+-------------------------+
 *	| RX driver info          |
 *	| depends on driver cfg   |
 *	+-------------------------+
 *	| RX paylad               |
 *	|                         |
 *	+-------------------------+
 *
 * Parameter:
 *	d	pointer to struct dvobj_priv of driver
 *	sz	rx driver info size in bytes.
 *
 * Rteurn:
 *	0	Success
 *	other	Fail
 */
int rtw_halmac_get_drv_info_sz(struct dvobj_priv *d, u8 *sz)
{
	HALMAC_RET_STATUS status;
	PHALMAC_ADAPTER halmac = dvobj_to_halmac(d);
	PHALMAC_API api = HALMAC_GET_API(halmac);
	u8 dw = 0;

	status = api->halmac_get_hw_value(halmac, HALMAC_HW_DRV_INFO_SIZE, &dw);
	if (status != HALMAC_RET_SUCCESS)
		return -1;

	*sz = dw * 8;
	return 0;
}
int rtw_halmac_get_rsvd_drv_pg_bndy(struct dvobj_priv *dvobj, u16 *drv_pg)
{
	HALMAC_RET_STATUS status;
	PHALMAC_ADAPTER halmac = dvobj_to_halmac(dvobj);
	PHALMAC_API api = HALMAC_GET_API(halmac);

	status = api->halmac_get_hw_value(halmac, HALMAC_HW_RSVD_PG_BNDY, drv_pg);
	if (status != HALMAC_RET_SUCCESS)
		return -1;

	return 0;
}

/*
 * Description
 *	Fill following spec info from HALMAC API:
 *	sec_cam_ent_num
 *
 * Return
 *	0	Success
 *	others	Fail
 */
int rtw_halmac_fill_hal_spec(struct dvobj_priv *dvobj, struct hal_spec_t *spec)
{
	HALMAC_RET_STATUS status;
	PHALMAC_ADAPTER halmac;
	PHALMAC_API api;
	u8 cam = 0;	/* Security Cam Entry Number */

	halmac = dvobj_to_halmac(dvobj);
	api = HALMAC_GET_API(halmac);

	/* Prepare data from HALMAC */
	status = api->halmac_get_hw_value(halmac, HALMAC_HW_CAM_ENTRY_NUM, &cam);
	if (status != HALMAC_RET_SUCCESS)
		return -1;

	/* Fill data to hal_spec_t */
	spec->sec_cam_ent_num = cam;

	return 0;
}

int rtw_halmac_p2pps(struct dvobj_priv *dvobj, PHAL_P2P_PS_PARA pp2p_ps_para)
{
	HALMAC_RET_STATUS status = HALMAC_RET_SUCCESS;
	PHALMAC_ADAPTER halmac = dvobj_to_halmac(dvobj);
	PHALMAC_API api = HALMAC_GET_API(halmac);
	HALMAC_P2PPS halmac_p2p_ps;

	(&halmac_p2p_ps)->offload_en = pp2p_ps_para->offload_en;
	(&halmac_p2p_ps)->role = pp2p_ps_para->role;
	(&halmac_p2p_ps)->ctwindow_en = pp2p_ps_para->ctwindow_en;
	(&halmac_p2p_ps)->noa_en = pp2p_ps_para->noa_en;
	(&halmac_p2p_ps)->noa_sel = pp2p_ps_para->noa_sel;
	(&halmac_p2p_ps)->all_sta_sleep = pp2p_ps_para->all_sta_sleep;
	(&halmac_p2p_ps)->discovery = pp2p_ps_para->discovery;
	(&halmac_p2p_ps)->p2p_port_id = _hw_port_drv2halmac(pp2p_ps_para->p2p_port_id);
	(&halmac_p2p_ps)->p2p_group = pp2p_ps_para->p2p_group;
	(&halmac_p2p_ps)->p2p_macid = pp2p_ps_para->p2p_macid;
	(&halmac_p2p_ps)->ctwindow_length = pp2p_ps_para->ctwindow_length;
	(&halmac_p2p_ps)->noa_duration_para = pp2p_ps_para->noa_duration_para;
	(&halmac_p2p_ps)->noa_interval_para = pp2p_ps_para->noa_interval_para;
	(&halmac_p2p_ps)->noa_start_time_para = pp2p_ps_para->noa_start_time_para;
	(&halmac_p2p_ps)->noa_count_para = pp2p_ps_para->noa_count_para;

	status = api->halmac_p2pps(halmac, (&halmac_p2p_ps));
	if (status != HALMAC_RET_SUCCESS)
		return -1;

	return 0;

}

