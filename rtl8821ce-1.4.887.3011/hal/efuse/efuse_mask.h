/* SPDX-License-Identifier: GPL-2.0 */

#if DEV_BUS_TYPE == RT_USB_INTERFACE

		#include "rtl8821c/HalEfuseMask8821C_USB.h"

#elif DEV_BUS_TYPE == RT_PCI_INTERFACE

		#include "rtl8821c/HalEfuseMask8821C_PCIE.h"

#elif DEV_BUS_TYPE == RT_SDIO_INTERFACE

		#include "rtl8821c/HalEfuseMask8821C_SDIO.h"

#endif
