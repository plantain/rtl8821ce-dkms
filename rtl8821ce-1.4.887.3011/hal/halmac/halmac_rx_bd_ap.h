/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _HALMAC_RX_BD_AP_H_
#define _HALMAC_RX_BD_AP_H_

/*TXBD_DW0*/

#define GET_RX_BD_RXFAIL(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x1, 31)
#define GET_RX_BD_TOTALRXPKTSIZE(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x1fff, 16)
#define GET_RX_BD_RXTAG(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x1fff, 16)
#define GET_RX_BD_FS(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x1, 15)
#define GET_RX_BD_LS(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x1, 14)
#define GET_RX_BD_RXBUFFSIZE(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword0, 0x3fff, 0)

/*TXBD_DW1*/

#define GET_RX_BD_PHYSICAL_ADDR_LOW(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword1, 0xffffffff, 0)

/*TXBD_DW2*/

#define GET_RX_BD_PHYSICAL_ADDR_HIGH(__pRxBd)    HALMAC_GET_BD_FIELD(((PHALMAC_RX_BD)__pRxBd)->Dword2, 0xffffffff, 0)

#endif
