/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/

#include <drv_types.h>
#include <rtl8821c_hal.h>
	
static void dm_CheckStatistics(PADAPTER adapter)
{
}
	
/*
 * ============================================================
 * functions
 * ============================================================
 */
static void init_phydm_cominfo(PADAPTER adapter)
{
	PHAL_DATA_TYPE hal;
	struct PHY_DM_STRUCT *pDM_Odm;
	u32 SupportAbility = 0;
	u8 cut_ver = ODM_CUT_A, fab_ver = ODM_TSMC;

	
	hal = GET_HAL_DATA(adapter);
	pDM_Odm = &hal->odmpriv;

	Init_ODM_ComInfo(adapter);

	odm_cmn_info_init(pDM_Odm, ODM_CMNINFO_PACKAGE_TYPE, hal->PackageType);

	if (IS_CHIP_VENDOR_TSMC(hal->version_id))
		fab_ver = ODM_TSMC;
	else if (IS_CHIP_VENDOR_UMC(hal->version_id))
		fab_ver = ODM_UMC;
	else if (IS_CHIP_VENDOR_SMIC(hal->version_id))
		fab_ver = ODM_UMC + 1;
	else
		RTW_INFO("%s: unknown fab_ver=%d !!\n",
			 __FUNCTION__, GET_CVID_MANUFACTUER(hal->version_id));

	if (IS_A_CUT(hal->version_id))
		cut_ver = ODM_CUT_A;
	else if (IS_B_CUT(hal->version_id))
		cut_ver = ODM_CUT_B;
	else if (IS_C_CUT(hal->version_id))
		cut_ver = ODM_CUT_C;
	else if (IS_D_CUT(hal->version_id))
		cut_ver = ODM_CUT_D;
	else if (IS_E_CUT(hal->version_id))
		cut_ver = ODM_CUT_E;
	else if (IS_F_CUT(hal->version_id))
		cut_ver = ODM_CUT_F;
	else if (IS_I_CUT(hal->version_id))
		cut_ver = ODM_CUT_I;
	else if (IS_J_CUT(hal->version_id))
		cut_ver = ODM_CUT_J;
	else if (IS_K_CUT(hal->version_id))
		cut_ver = ODM_CUT_K;
	else
		RTW_INFO("%s: unknown cut_ver=%d !!\n",
			 __FUNCTION__, GET_CVID_CUT_VERSION(hal->version_id));

	RTW_INFO("%s: fab_ver=%d cut_ver=%d\n", __FUNCTION__, fab_ver, cut_ver);
	odm_cmn_info_init(pDM_Odm, ODM_CMNINFO_FAB_VER, fab_ver);
	odm_cmn_info_init(pDM_Odm, ODM_CMNINFO_CUT_VER, cut_ver);

	SupportAbility = ODM_RF_CALIBRATION | ODM_RF_TX_PWR_TRACK;

	odm_cmn_info_update(pDM_Odm, ODM_CMNINFO_ABILITY, SupportAbility);
}
	
void rtl8821c_phy_init_dm_priv(PADAPTER adapter)
{
	PHAL_DATA_TYPE hal = GET_HAL_DATA(adapter);
	struct PHY_DM_STRUCT *podmpriv = &hal->odmpriv;

	init_phydm_cominfo(adapter);

	/*PHYDM API - thermal trim*/
	phydm_get_thermal_trim_offset(podmpriv);
	/*PHYDM API - power trim*/
	phydm_get_power_trim_offset(podmpriv);
}
	
void rtl8821c_phy_init_haldm(PADAPTER adapter)
{
	PHAL_DATA_TYPE hal = GET_HAL_DATA(adapter);
	struct PHY_DM_STRUCT *pDM_Odm = &hal->odmpriv;

	hal->DM_Type = dm_type_by_driver;
	odm_dm_init(pDM_Odm);
}

void rtl8821c_phy_haldm_watchdog(PADAPTER Adapter)
{
	BOOLEAN bFwCurrentInPSMode = _FALSE;
	BOOLEAN bFwPSAwake = _TRUE;
	PHAL_DATA_TYPE	pHalData = GET_HAL_DATA(Adapter);

	if (!rtw_is_hw_init_completed(Adapter))
		goto skip_dm;

	bFwCurrentInPSMode = adapter_to_pwrctl(Adapter)->bFwCurrentInPSMode;
	rtw_hal_get_hwreg(Adapter, HW_VAR_FWLPS_RF_ON, (u8 *)(&bFwPSAwake));

	/* Fw is under p2p powersaving mode, driver should stop dynamic mechanism.
	 modifed by thomas. 2011.06.11.*/
	if (Adapter->wdinfo.p2p_ps_mode)
		bFwPSAwake = _FALSE;

	if ((rtw_is_hw_init_completed(Adapter))
		&& ((!bFwCurrentInPSMode) && bFwPSAwake)) {
		/*	Calculate Tx/Rx statistics. */
		dm_CheckStatistics(Adapter);

		/* Dynamically switch RTS/CTS protection.*/
		/*dm_CheckProtection(Adapter);*/
	}


	if (rtw_is_hw_init_completed(Adapter)) {
		u8	bLinked = _FALSE;
		u8	bsta_state = _FALSE;
		u8	bBtDisabled = _TRUE;

		if (rtw_mi_check_status(Adapter, MI_STA_LINKED) || rtw_mi_check_status(Adapter, MI_AP_ASSOC)) {
			bLinked = _TRUE;
			if (rtw_mi_check_status(Adapter, MI_STA_LINKED))
				bsta_state = _TRUE;
		}

		odm_cmn_info_update(&pHalData->odmpriv, ODM_CMNINFO_LINK, bLinked);
		odm_cmn_info_update(&pHalData->odmpriv, ODM_CMNINFO_STATION_STATE, bsta_state);

		bBtDisabled = rtw_btcoex_IsBtDisabled(Adapter);
		odm_cmn_info_update(&pHalData->odmpriv, ODM_CMNINFO_BT_ENABLED, ((bBtDisabled == _TRUE) ? _FALSE : _TRUE));

		odm_dm_watchdog(&pHalData->odmpriv);

	}

skip_dm:

	return;
}

void rtl8821c_phy_haldm_in_lps(PADAPTER adapter)
{
}
