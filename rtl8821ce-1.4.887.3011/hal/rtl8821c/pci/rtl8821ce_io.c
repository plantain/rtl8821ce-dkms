/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2015 - 2016 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
#define _RTL8821CE_IO_C_

#include <drv_types.h>		/* PADAPTER and etc. */


static u8 pci_read8(struct intf_hdl *phdl, u32 addr)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	return 0xff & readb((u8 *)pdvobjpriv->pci_mem_start + addr);
}

static u16 pci_read16(struct intf_hdl *phdl, u32 addr)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	return readw((u8 *)pdvobjpriv->pci_mem_start + addr);
}

static u32 pci_read32(struct intf_hdl *phdl, u32 addr)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	return readl((u8 *)pdvobjpriv->pci_mem_start + addr);
}

/*
 * 2009.12.23. by tynli. Suggested by SD1 victorh.
 * For ASPM hang on AMD and Nvidia.
 * 20100212 Tynli: Do read IO operation after write for
 * all PCI bridge suggested by SD1. Origianally this is only for INTEL.
 */
static int pci_write8(struct intf_hdl *phdl, u32 addr, u8 val)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	writeb(val, (u8 *)pdvobjpriv->pci_mem_start + addr);
	return 1;
}

static int pci_write16(struct intf_hdl *phdl, u32 addr, u16 val)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	writew(val, (u8 *)pdvobjpriv->pci_mem_start + addr);
	return 2;
}

static int pci_write32(struct intf_hdl *phdl, u32 addr, u32 val)
{
	struct dvobj_priv *pdvobjpriv = (struct dvobj_priv *)phdl->pintf_dev;

	writel(val, (u8 *)pdvobjpriv->pci_mem_start + addr);
	return 4;
}

static void pci_read_mem(struct intf_hdl *phdl, u32 addr, u32 cnt, u8 *rmem)
{
	RTW_INFO("%s(%d)fake function\n", __func__, __LINE__);
}

static void pci_write_mem(struct intf_hdl *phdl, u32 addr, u32 cnt, u8 *wmem)
{
	RTW_INFO("%s(%d)fake function\n", __func__, __LINE__);
}

static u32 pci_read_port(struct intf_hdl *phdl, u32 addr, u32 cnt, u8 *rmem)
{
	return 0;
}

static u32 pci_write_port(struct intf_hdl *phdl, u32 addr, u32 cnt, u8 *wmem)
{
	_adapter *padapter = (_adapter *)phdl->padapter;

	netif_trans_update(padapter->pnetdev);

	return 0;
}

void rtl8821ce_set_intf_ops(struct _io_ops *pops)
{

	_rtw_memset((u8 *)pops, 0, sizeof(struct _io_ops));

	pops->_read8 = &pci_read8;
	pops->_read16 = &pci_read16;
	pops->_read32 = &pci_read32;

	pops->_read_mem = &pci_read_mem;
	pops->_read_port = &pci_read_port;

	pops->_write8 = &pci_write8;
	pops->_write16 = &pci_write16;
	pops->_write32 = &pci_write32;

	pops->_write_mem = &pci_write_mem;
	pops->_write_port = &pci_write_port;

}
