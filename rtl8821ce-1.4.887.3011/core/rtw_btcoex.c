/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2013 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/

#include <drv_types.h>
#include <hal_btcoex.h>
#include <hal_data.h>

void rtw_btcoex_Initialize(PADAPTER padapter)
{
	hal_btcoex_Initialize(padapter);
}

void rtw_btcoex_PowerOnSetting(PADAPTER padapter)
{
	hal_btcoex_PowerOnSetting(padapter);
}

void rtw_btcoex_PowerOffSetting(PADAPTER padapter)
{
	hal_btcoex_PowerOffSetting(padapter);
}

void rtw_btcoex_HAL_Initialize(PADAPTER padapter, u8 bWifiOnly)
{
	hal_btcoex_InitHwConfig(padapter, bWifiOnly);
}

void rtw_btcoex_IpsNotify(PADAPTER padapter, u8 type)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_IpsNotify(padapter, type);
}

void rtw_btcoex_LpsNotify(PADAPTER padapter, u8 type)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_LpsNotify(padapter, type);
}

void rtw_btcoex_ScanNotify(PADAPTER padapter, u8 type)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	if (_FALSE == type) {

		if (DEV_MGMT_TX_NUM(adapter_to_dvobj(padapter))
			|| DEV_ROCH_NUM(adapter_to_dvobj(padapter)))
			return;
	}

	hal_btcoex_ScanNotify(padapter, type);
}

void rtw_btcoex_ConnectNotify(PADAPTER padapter, u8 action)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;



	hal_btcoex_ConnectNotify(padapter, action);
}

void rtw_btcoex_MediaStatusNotify(PADAPTER padapter, u8 mediaStatus)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;



	if ((RT_MEDIA_CONNECT == mediaStatus)
	    && (check_fwstate(&padapter->mlmepriv, WIFI_AP_STATE) == _TRUE))
		rtw_hal_set_hwreg(padapter, HW_VAR_DL_RSVD_PAGE, NULL);

	hal_btcoex_MediaStatusNotify(padapter, mediaStatus);
}

void rtw_btcoex_SpecialPacketNotify(PADAPTER padapter, u8 pktType)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_SpecialPacketNotify(padapter, pktType);
}

void rtw_btcoex_BtInfoNotify(PADAPTER padapter, u8 length, u8 *tmpBuf)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_BtInfoNotify(padapter, length, tmpBuf);
}

void rtw_btcoex_BtMpRptNotify(PADAPTER padapter, u8 length, u8 *tmpBuf)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	if (padapter->registrypriv.mp_mode == 1)
		return;

	hal_btcoex_BtMpRptNotify(padapter, length, tmpBuf);
}

void rtw_btcoex_SuspendNotify(PADAPTER padapter, u8 state)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_SuspendNotify(padapter, state);
}

void rtw_btcoex_HaltNotify(PADAPTER padapter)
{
	PHAL_DATA_TYPE	pHalData;
	u8 do_halt = 1;

	pHalData = GET_HAL_DATA(padapter);
	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		do_halt = 0;

	if (_FALSE == padapter->bup) {
		RTW_INFO(FUNC_ADPT_FMT ": bup=%d Skip!\n",
			 FUNC_ADPT_ARG(padapter), padapter->bup);
		do_halt = 0;
	}

	if (rtw_is_surprise_removed(padapter)) {
		RTW_INFO(FUNC_ADPT_FMT ": bSurpriseRemoved=%s Skip!\n",
			FUNC_ADPT_ARG(padapter), rtw_is_surprise_removed(padapter) ? "True" : "False");
		do_halt = 0;
	}

	hal_btcoex_HaltNotify(padapter, do_halt);
}

void rtw_btcoex_switchband_notify(u8 under_scan, u8 band_type)
{
	hal_btcoex_switchband_notify(under_scan, band_type);
}

u8 rtw_btcoex_IsBtDisabled(PADAPTER padapter)
{
	return hal_btcoex_IsBtDisabled(padapter);
}

void rtw_btcoex_Handler(PADAPTER padapter)
{
	PHAL_DATA_TYPE	pHalData;

	pHalData = GET_HAL_DATA(padapter);

	if (_FALSE == pHalData->EEPROMBluetoothCoexist)
		return;

	hal_btcoex_Hanlder(padapter);
}

s32 rtw_btcoex_IsBTCoexRejectAMPDU(PADAPTER padapter)
{
	s32 coexctrl;

	coexctrl = hal_btcoex_IsBTCoexRejectAMPDU(padapter);

	return coexctrl;
}

s32 rtw_btcoex_IsBTCoexCtrlAMPDUSize(PADAPTER padapter)
{
	s32 coexctrl;

	coexctrl = hal_btcoex_IsBTCoexCtrlAMPDUSize(padapter);

	return coexctrl;
}

u32 rtw_btcoex_GetAMPDUSize(PADAPTER padapter)
{
	u32 size;

	size = hal_btcoex_GetAMPDUSize(padapter);

	return size;
}

void rtw_btcoex_SetManualControl(PADAPTER padapter, u8 manual)
{
	if (_TRUE == manual)
		hal_btcoex_SetManualControl(padapter, _TRUE);
	else
		hal_btcoex_SetManualControl(padapter, _FALSE);
}

u8 rtw_btcoex_IsBtControlLps(PADAPTER padapter)
{
	return hal_btcoex_IsBtControlLps(padapter);
}

u8 rtw_btcoex_IsLpsOn(PADAPTER padapter)
{
	return hal_btcoex_IsLpsOn(padapter);
}

u8 rtw_btcoex_RpwmVal(PADAPTER padapter)
{
	return hal_btcoex_RpwmVal(padapter);
}

u8 rtw_btcoex_LpsVal(PADAPTER padapter)
{
	return hal_btcoex_LpsVal(padapter);
}

u32 rtw_btcoex_GetRaMask(PADAPTER padapter)
{
	return hal_btcoex_GetRaMask(padapter);
}

void rtw_btcoex_RecordPwrMode(PADAPTER padapter, u8 *pCmdBuf, u8 cmdLen)
{
	hal_btcoex_RecordPwrMode(padapter, pCmdBuf, cmdLen);
}

/* ==================================================
 * Below Functions are called by BT-Coex
 * ================================================== */
void rtw_btcoex_rx_ampdu_apply(PADAPTER padapter)
{
	rtw_rx_ampdu_apply(padapter);
}

void rtw_btcoex_LPS_Enter(PADAPTER padapter)
{
	struct pwrctrl_priv *pwrpriv;
	u8 lpsVal;

	pwrpriv = adapter_to_pwrctl(padapter);

	pwrpriv->bpower_saving = _TRUE;
	lpsVal = rtw_btcoex_LpsVal(padapter);
	rtw_set_ps_mode(padapter, PS_MODE_MIN, 0, lpsVal, "BTCOEX");
}

u8 rtw_btcoex_LPS_Leave(PADAPTER padapter)
{
	struct pwrctrl_priv *pwrpriv;

	pwrpriv = adapter_to_pwrctl(padapter);

	if (pwrpriv->pwr_mode != PS_MODE_ACTIVE) {
		rtw_set_ps_mode(padapter, PS_MODE_ACTIVE, 0, 0, "BTCOEX");
		LPS_RF_ON_check(padapter, 100);
		pwrpriv->bpower_saving = _FALSE;
	}

	return _TRUE;
}

u8 rtw_btcoex_get_bt_coexist(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	return pHalData->EEPROMBluetoothCoexist;
}

u8 rtw_btcoex_get_chip_type(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	return pHalData->EEPROMBluetoothType;
}

u8 rtw_btcoex_get_pg_ant_num(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	return pHalData->EEPROMBluetoothAntNum == Ant_x2 ? 2 : 1;
}

u8 rtw_btcoex_get_pg_single_ant_path(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	return pHalData->ant_path;
}

u8 rtw_btcoex_get_pg_rfe_type(PADAPTER padapter)
{
	HAL_DATA_TYPE	*pHalData = GET_HAL_DATA(padapter);

	return pHalData->rfe_type;
}

u8 rtw_btcoex_is_tfbga_package_type(PADAPTER padapter)
{
	return _FALSE;
}

u8 rtw_btcoex_get_ant_div_cfg(PADAPTER padapter)
{
	PHAL_DATA_TYPE pHalData;

	pHalData = GET_HAL_DATA(padapter);
	
	return (pHalData->AntDivCfg == 0) ? _FALSE : _TRUE;
}
