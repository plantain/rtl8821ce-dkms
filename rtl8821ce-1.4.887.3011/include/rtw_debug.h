/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
#ifndef __RTW_DEBUG_H__
#define __RTW_DEBUG_H__

/* driver log level*/
enum {
	_DRV_NONE_ = 0,
	_DRV_ALWAYS_ = 1,
	_DRV_ERR_ = 2,
	_DRV_WARNING_ = 3,
	_DRV_INFO_ = 4,
	_DRV_DEBUG_ = 5,
	_DRV_MAX_ = 6
};

#define DRIVER_PREFIX "RTW: "

#define RTW_PRINT(x, ...) do {} while (0)
#define RTW_ERR(x, ...) do {} while (0)
#define RTW_WARN(x,...) do {} while (0)
#define RTW_INFO(x,...) do {} while (0)
#define RTW_DBG(x,...) do {} while (0)
#define RTW_PRINT_SEL(x,...) do {} while (0)
#define _RTW_PRINT(x, ...) do {} while (0)
#define _RTW_ERR(x, ...) do {} while (0)
#define _RTW_WARN(x,...) do {} while (0)
#define _RTW_INFO(x,...) do {} while (0)
#define _RTW_DBG(x,...) do {} while (0)
#define _RTW_PRINT_SEL(x,...) do {} while (0)

#define RTW_INFO_DUMP(_TitleString, _HexData, _HexDataLen) do {} while (0)
#define RTW_DBG_DUMP(_TitleString, _HexData, _HexDataLen) do {} while (0)
#define RTW_PRINT_DUMP(_TitleString, _HexData, _HexDataLen) do {} while (0)
#define _RTW_INFO_DUMP(_TitleString, _HexData, _HexDataLen) do {} while (0)
#define _RTW_DBG_DUMP(_TitleString, _HexData, _HexDataLen) do {} while (0)

#define RTW_DBG_EXPR(EXPR) do {} while (0)

#define RTW_DBGDUMP 0 /* 'stream' for _dbgdump */

/* don't use these 3 APIs anymore, will be removed later */
#define RT_TRACE(_Comp, _Level, Fmt) do {} while (0)

#undef _dbgdump
#undef _seqdump

#define _dbgdump printk
#define _seqdump seq_printf

#ifndef _OS_INTFS_C_
extern uint rtw_drv_log_level;
#endif

#if defined(_dbgdump)

/* with driver-defined prefix */
#undef RTW_PRINT
#define RTW_PRINT(fmt, arg...)     \
	do {\
		if (_DRV_ALWAYS_ <= rtw_drv_log_level) {\
			_dbgdump(DRIVER_PREFIX fmt, ##arg);\
		} \
	} while (0)

#undef RTW_ERR
#define RTW_ERR(fmt, arg...)     \
	do {\
		if (_DRV_ERR_ <= rtw_drv_log_level) {\
			_dbgdump(DRIVER_PREFIX"ERROR " fmt, ##arg);\
		} \
	} while (0)

#undef RTW_WARN
#define RTW_WARN(fmt, arg...)     \
	do {\
		if (_DRV_WARNING_ <= rtw_drv_log_level) {\
			_dbgdump(DRIVER_PREFIX"WARN " fmt, ##arg);\
		} \
	} while (0)

#undef RTW_INFO
#define RTW_INFO(fmt, arg...)     \
	do {\
		if (_DRV_INFO_ <= rtw_drv_log_level) {\
			_dbgdump(DRIVER_PREFIX fmt, ##arg);\
		} \
	} while (0)

#undef RTW_DBG
#define RTW_DBG(fmt, arg...)     \
	do {\
		if (_DRV_DEBUG_ <= rtw_drv_log_level) {\
			_dbgdump(DRIVER_PREFIX fmt, ##arg);\
		} \
	} while (0)

#undef RTW_INFO_DUMP
#define RTW_INFO_DUMP(_TitleString, _HexData, _HexDataLen)			\
	do {\
		if (_DRV_INFO_ <= rtw_drv_log_level) {	\
			int __i;								\
			u8	*ptr = (u8 *)_HexData;				\
			_dbgdump("%s", DRIVER_PREFIX);						\
			_dbgdump(_TitleString);						\
			for (__i = 0; __i < (int)_HexDataLen; __i++) {				\
				_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
				if (((__i + 1) % 16) == 0)	\
					_dbgdump("\n");			\
			}								\
			_dbgdump("\n");							\
		} \
	} while (0)

#undef RTW_DBG_DUMP
#define RTW_DBG_DUMP(_TitleString, _HexData, _HexDataLen)			\
	do {\
		if (_DRV_DEBUG_ <= rtw_drv_log_level) { \
			int __i;								\
			u8	*ptr = (u8 *)_HexData;				\
			_dbgdump("%s", DRIVER_PREFIX);						\
			_dbgdump(_TitleString);						\
			for (__i = 0; __i < (int)_HexDataLen; __i++) {				\
				_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
				if (((__i + 1) % 16) == 0)	\
					_dbgdump("\n");			\
			}								\
			_dbgdump("\n");							\
		} \
	} while (0)

#undef RTW_PRINT_DUMP
#define RTW_PRINT_DUMP(_TitleString, _HexData, _HexDataLen)			\
	do {\
		if (_DRV_ALWAYS_ <= rtw_drv_log_level) { \
			int __i;								\
			u8	*ptr = (u8 *)_HexData;				\
			_dbgdump("%s", DRIVER_PREFIX);						\
			_dbgdump(_TitleString); 					\
			for (__i = 0; __i < (int)_HexDataLen; __i++) {				\
				_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
				if (((__i + 1) % 16) == 0)	\
					_dbgdump("\n"); 		\
			}								\
			_dbgdump("\n"); 						\
		} \
	} while (0)

/* without driver-defined prefix */
#undef _RTW_PRINT
#define _RTW_PRINT(fmt, arg...)     \
	do {\
		if (_DRV_ALWAYS_ <= rtw_drv_log_level) {\
			_dbgdump(fmt, ##arg);\
		} \
	} while (0)

#undef _RTW_ERR
#define _RTW_ERR(fmt, arg...)     \
	do {\
		if (_DRV_ERR_ <= rtw_drv_log_level) {\
			_dbgdump(fmt, ##arg);\
		} \
	} while (0)

#undef _RTW_WARN
#define _RTW_WARN(fmt, arg...)     \
	do {\
		if (_DRV_WARNING_ <= rtw_drv_log_level) {\
			_dbgdump(fmt, ##arg);\
		} \
	} while (0)

#undef _RTW_INFO
#define _RTW_INFO(fmt, arg...)     \
	do {\
		if (_DRV_INFO_ <= rtw_drv_log_level) {\
			_dbgdump(fmt, ##arg);\
		} \
	} while (0)

#undef _RTW_DBG
#define _RTW_DBG(fmt, arg...)     \
	do {\
		if (_DRV_DEBUG_ <= rtw_drv_log_level) {\
			_dbgdump(fmt, ##arg);\
		} \
	} while (0)

#undef _RTW_INFO_DUMP
#define _RTW_INFO_DUMP(_TitleString, _HexData, _HexDataLen)			\
	if (_DRV_INFO_ <= rtw_drv_log_level) {	\
		int __i;								\
		u8	*ptr = (u8 *)_HexData;				\
		_dbgdump(_TitleString);						\
		for (__i = 0; __i<(int)_HexDataLen; __i++)				\
		{								\
			_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
			if (((__i + 1) % 16) == 0)	_dbgdump("\n");			\
		}								\
		_dbgdump("\n");							\
	}

#undef _RTW_DBG_DUMP
#define _RTW_DBG_DUMP(_TitleString, _HexData, _HexDataLen)			\
	if (_DRV_DEBUG_ <= rtw_drv_log_level) { \
		int __i;								\
		u8	*ptr = (u8 *)_HexData;				\
		_dbgdump(_TitleString);						\
		for (__i = 0; __i<(int)_HexDataLen; __i++)				\
		{								\
			_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
			if (((__i + 1) % 16) == 0)	_dbgdump("\n");			\
		}								\
		_dbgdump("\n");							\
	}

/* other debug APIs */
#undef RTW_DBG_EXPR
#define RTW_DBG_EXPR(EXPR) do { if (_DRV_DEBUG_ <= rtw_drv_log_level) EXPR; } while (0)

#endif /* defined(_dbgdump) */

#if defined(_seqdump)
/* dump message to selected 'stream' with driver-defined prefix */
#undef RTW_PRINT_SEL
#define RTW_PRINT_SEL(sel, fmt, arg...) \
	do {\
		if (sel == RTW_DBGDUMP)\
			RTW_PRINT(fmt, ##arg); \
		else {\
			_seqdump(sel, fmt, ##arg) /*rtw_warn_on(1)*/; \
		} \
	} while (0)

/* dump message to selected 'stream' */
#undef _RTW_PRINT_SEL
#define _RTW_PRINT_SEL(sel, fmt, arg...) \
	do {\
		if (sel == RTW_DBGDUMP)\
			_RTW_PRINT(fmt, ##arg); \
		else {\
			_seqdump(sel, fmt, ##arg) /*rtw_warn_on(1)*/; \
		} \
	} while (0)

/* dump message to selected 'stream' */
#undef _RTW_DUMP_SEL
#define _RTW_DUMP_SEL(sel, _HexData, _HexDataLen) \
	do {\
		if (sel == RTW_DBGDUMP) {\
			int __i;								\
			u8	*ptr = (u8 *)_HexData;				\
			for (__i = 0; __i < (int)_HexDataLen; __i++) {				\
				_dbgdump("%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
				if (((__i + 1) % 16) == 0)	\
					_dbgdump("\n");			\
			}								\
			_dbgdump("\n");							\
		} \
		else {\
			int __i;								\
			u8	*ptr = (u8 *)_HexData;				\
			for (__i = 0; __i < (int)_HexDataLen; __i++) {				\
				_seqdump(sel, "%02X%s", ptr[__i], (((__i + 1) % 4) == 0) ? "  " : " ");	\
				if (((__i + 1) % 16) == 0)	\
					_seqdump(sel, "\n");			\
			}								\
			_seqdump(sel, "\n");							\
		} \
	} while (0)

#endif /* defined(_seqdump) */

#ifdef CONFIG_DBG_COUNTER
	#define DBG_COUNTER(counter) counter++
#else
	#define DBG_COUNTER(counter)
#endif

void dump_drv_version(void *sel);
void dump_log_level(void *sel);
void dump_drv_cfg(void *sel);

void mac_reg_dump(void *sel, _adapter *adapter);
void bb_reg_dump(void *sel, _adapter *adapter);
void bb_reg_dump_ex(void *sel, _adapter *adapter);
void rf_reg_dump(void *sel, _adapter *adapter);

void rtw_sink_rtp_seq_dbg(_adapter *adapter, _pkt *pkt);

struct sta_info;
void sta_rx_reorder_ctl_dump(void *sel, struct sta_info *sta);

struct dvobj_priv;
void dump_tx_rate_bmp(void *sel, struct dvobj_priv *dvobj);
void dump_adapters_status(void *sel, struct dvobj_priv *dvobj);

struct sec_cam_ent;
void dump_sec_cam_ent(void *sel, struct sec_cam_ent *ent, int id);
void dump_sec_cam_ent_title(void *sel, u8 has_id);
void dump_sec_cam(void *sel, _adapter *adapter);
void dump_sec_cam_cache(void *sel, _adapter *adapter);

bool rtw_fwdl_test_trigger_chksum_fail(void);
bool rtw_fwdl_test_trigger_wintint_rdy_fail(void);
bool rtw_del_rx_ampdu_test_trigger_no_tx_fail(void);
u32 rtw_get_wait_hiq_empty_ms(void);
void rtw_sta_linking_test_set_start(void);
bool rtw_sta_linking_test_wait_done(void);
bool rtw_sta_linking_test_force_fail(void);

void dump_regsty_rx_ampdu_size_limit(void *sel, _adapter *adapter);

void rtw_dump_dft_phy_cap(void *sel, _adapter *adapter);
void rtw_get_dft_phy_cap(void *sel, _adapter *adapter);
void rtw_dump_drv_phy_cap(void *sel, _adapter *adapter);

#endif /* __RTW_DEBUG_H__ */
