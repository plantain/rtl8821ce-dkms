/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
#ifndef _RTW_XMIT_H_
#define _RTW_XMIT_H_

	#define MAX_XMITBUF_SZ	(1664)
	#define NR_XMITBUFF	(128)

	#define XMITBUF_ALIGN_SZ 4

/* xmit extension buff defination */
#define MAX_XMIT_EXTBUF_SZ	(1536)

#ifdef CONFIG_SINGLE_XMIT_BUF
	#define NR_XMIT_EXTBUFF	(1)
#else
	#define NR_XMIT_EXTBUFF	(32)
#endif

#define MAX_CMDBUF_SZ	(5120)	/* (4096) */

#define MAX_NUMBLKS		(1)

#define XMIT_VO_QUEUE (0)
#define XMIT_VI_QUEUE (1)
#define XMIT_BE_QUEUE (2)
#define XMIT_BK_QUEUE (3)

#define VO_QUEUE_INX		0
#define VI_QUEUE_INX		1
#define BE_QUEUE_INX		2
#define BK_QUEUE_INX		3
#define BCN_QUEUE_INX		4
#define MGT_QUEUE_INX		5
#define HIGH_QUEUE_INX		6
#define TXCMD_QUEUE_INX	7

#define HW_QUEUE_ENTRY	8

		#define TX_BD_NUM			(128+1)	/* +1 result from ring buffer */

#define WEP_IV(pattrib_iv, dot11txpn, keyidx)\
	do {\
		pattrib_iv[0] = dot11txpn._byte_.TSC0;\
		pattrib_iv[1] = dot11txpn._byte_.TSC1;\
		pattrib_iv[2] = dot11txpn._byte_.TSC2;\
		pattrib_iv[3] = ((keyidx & 0x3)<<6);\
		dot11txpn.val = (dot11txpn.val == 0xffffff) ? 0 : (dot11txpn.val+1);\
	} while (0)

#define TKIP_IV(pattrib_iv, dot11txpn, keyidx)\
	do {\
		pattrib_iv[0] = dot11txpn._byte_.TSC1;\
		pattrib_iv[1] = (dot11txpn._byte_.TSC1 | 0x20) & 0x7f;\
		pattrib_iv[2] = dot11txpn._byte_.TSC0;\
		pattrib_iv[3] = BIT(5) | ((keyidx & 0x3)<<6);\
		pattrib_iv[4] = dot11txpn._byte_.TSC2;\
		pattrib_iv[5] = dot11txpn._byte_.TSC3;\
		pattrib_iv[6] = dot11txpn._byte_.TSC4;\
		pattrib_iv[7] = dot11txpn._byte_.TSC5;\
		dot11txpn.val = dot11txpn.val == 0xffffffffffffULL ? 0 : (dot11txpn.val+1);\
	} while (0)

#define AES_IV(pattrib_iv, dot11txpn, keyidx)\
	do {\
		pattrib_iv[0] = dot11txpn._byte_.TSC0;\
		pattrib_iv[1] = dot11txpn._byte_.TSC1;\
		pattrib_iv[2] = 0;\
		pattrib_iv[3] = BIT(5) | ((keyidx & 0x3)<<6);\
		pattrib_iv[4] = dot11txpn._byte_.TSC2;\
		pattrib_iv[5] = dot11txpn._byte_.TSC3;\
		pattrib_iv[6] = dot11txpn._byte_.TSC4;\
		pattrib_iv[7] = dot11txpn._byte_.TSC5;\
		dot11txpn.val = dot11txpn.val == 0xffffffffffffULL ? 0 : (dot11txpn.val+1);\
	} while (0)

/* Check if AMPDU Tx is supported or not. If it is supported,
* it need to check "amsdu in ampdu" is supported or not.
* (ampdu_en, amsdu_ampdu_en) =
* (0, x) : AMPDU is not enable, but AMSDU is valid to send.
* (1, 0) : AMPDU is enable, AMSDU in AMPDU is not enable. So, AMSDU is not valid to send.
* (1, 1) : AMPDU and AMSDU in AMPDU are enable. So, AMSDU is valid to send.
*/
#define IS_AMSDU_AMPDU_NOT_VALID(pattrib)\
	 ((pattrib->ampdu_en == _TRUE) && (pattrib->amsdu_ampdu_en == _FALSE))

#define IS_AMSDU_AMPDU_VALID(pattrib)\
	 !((pattrib->ampdu_en == _TRUE) && (pattrib->amsdu_ampdu_en == _FALSE))

#define HWXMIT_ENTRY	4

/* For Buffer Descriptor ring architecture */
	#define TX_BUFFER_SEG_NUM	1 /* 0:2 seg, 1: 4 seg, 2: 8 seg. */

#define TXDESC_SIZE 48		/* HALMAC_TX_DESC_SIZE_8821C */


		/* this section is defined for buffer descriptor ring architecture */
		#define TX_WIFI_INFO_SIZE (TXDESC_SIZE) /* it may add 802.11 hdr or others... */
		/* tx desc and payload are in the same buf */
		#define TXDESC_OFFSET (TX_WIFI_INFO_SIZE)

enum TXDESC_SC {
	SC_DONT_CARE = 0x00,
	SC_UPPER = 0x01,
	SC_LOWER = 0x02,
	SC_DUPLICATE = 0x03
};


struct tx_buf_desc {
#define TX_BUFFER_SEG_SIZE	2	/* in unit of DWORD */
	unsigned int dword[TX_BUFFER_SEG_SIZE * (2 << TX_BUFFER_SEG_NUM)];
} __packed;


#define PCI_MAX_TX_QUEUE_COUNT	8	/* == HW_QUEUE_ENTRY */

struct rtw_tx_ring {
	unsigned char	qid;
	struct tx_buf_desc	*buf_desc;
	dma_addr_t	dma;
	unsigned int	idx;
	unsigned int	entries;
	_queue		queue;
	u32		qlen;
	u16		hw_rp_cache;
};

struct	hw_xmit	{
	/* _lock xmit_lock; */
	/* _list	pending; */
	_queue *sta_queue;
	/* struct hw_txqueue *phwtxqueue; */
	/* sint	txcmdcnt; */
	int	accnt;
};

/* reduce size */
struct pkt_attrib {
	u8	type;
	u8	subtype;
	u8	bswenc;
	u8	dhcp_pkt;
	u16	ether_type;
	u16	seqnum;
	u8	hw_ssn_sel;	/* for HW_SEQ0,1,2,3 */
	u16	pkt_hdrlen;	/* the original 802.3 pkt header len */
	u16	hdrlen;		/* the WLAN Header Len */
	u32	pktlen;		/* the original 802.3 pkt raw_data len (not include ether_hdr data) */
	u32	last_txcmdsz;
	u8	nr_frags;
	u8	encrypt;	/* when 0 indicate no encrypt. when non-zero, indicate the encrypt algorith */
	u8	iv_len;
	u8	icv_len;
	u8	iv[18];
	u8	icv[16];
	u8	priority;
	u8	ack_policy;
	u8	mac_id;
	u8	vcs_mode;	/* virtual carrier sense method */
	u8	dst[ETH_ALEN];
	u8	src[ETH_ALEN];
	u8	ta[ETH_ALEN];
	u8	ra[ETH_ALEN];
	u8	key_idx;
	u8	qos_en;
	u8	ht_en;
	u8	raid;/* rate adpative id */
	u8	bwmode;
	u8	ch_offset;/* PRIME_CHNL_OFFSET */
	u8	sgi;/* short GI */
	u8	ampdu_en;/* tx ampdu enable */
	u8	ampdu_spacing; /* ampdu_min_spacing for peer sta's rx */
	u8	amsdu;
	u8	amsdu_ampdu_en;/* tx amsdu in ampdu enable */
	u8	mdata;/* more data bit */
	u8	pctrl;/* per packet txdesc control enable */
	u8	triggered;/* for ap mode handling Power Saving sta */
	u8	qsel;
	u8	order;/* order bit */
	u8	eosp;
	u8	rate;
	u8	intel_proxim;
	u8	retry_ctrl;
	u8   mbssid;
	u8	ldpc;
	u8	stbc;
	struct sta_info *psta;

	u8 rtsen;
	u8 cts2self;
	union Keytype	dot11tkiptxmickey;
	/* union Keytype	dot11tkiprxmickey; */
	union Keytype	dot118021x_UncstKey;

	u8 key_type;

	u8 icmp_pkt;


};


#define WLANHDR_OFFSET	64

#define NULL_FRAMETAG		(0x0)
#define DATA_FRAMETAG		0x01
#define L2_FRAMETAG		0x02
#define MGNT_FRAMETAG		0x03
#define AMSDU_FRAMETAG	0x04

#define EII_FRAMETAG		0x05
#define IEEE8023_FRAMETAG  0x06

#define MP_FRAMETAG		0x07

#define TXAGG_FRAMETAG	0x08

enum {
	XMITBUF_DATA = 0,
	XMITBUF_MGNT = 1,
	XMITBUF_CMD = 2,
};

bool rtw_xmit_ac_blocked(_adapter *adapter);

struct  submit_ctx {
	u32 submit_time; /* */
	u32 timeout_ms; /* <0: not synchronous, 0: wait forever, >0: up to ms waiting */
	int status; /* status for operation */
	struct completion done;
};

enum {
	RTW_SCTX_SUBMITTED = -1,
	RTW_SCTX_DONE_SUCCESS = 0,
	RTW_SCTX_DONE_UNKNOWN,
	RTW_SCTX_DONE_TIMEOUT,
	RTW_SCTX_DONE_BUF_ALLOC,
	RTW_SCTX_DONE_BUF_FREE,
	RTW_SCTX_DONE_WRITE_PORT_ERR,
	RTW_SCTX_DONE_TX_DESC_NA,
	RTW_SCTX_DONE_TX_DENY,
	RTW_SCTX_DONE_CCX_PKT_FAIL,
	RTW_SCTX_DONE_DRV_STOP,
	RTW_SCTX_DONE_DEV_REMOVE,
	RTW_SCTX_DONE_CMD_ERROR,
	RTW_SCTX_DONE_CMD_DROP,
	RTX_SCTX_CSTR_WAIT_RPT2,
};

void rtw_sctx_init(struct submit_ctx *sctx, int timeout_ms);
int rtw_sctx_wait(struct submit_ctx *sctx, const char *msg);
void rtw_sctx_done_err(struct submit_ctx **sctx, int status);
void rtw_sctx_done(struct submit_ctx **sctx);

struct xmit_buf {
	_list	list;

	_adapter *padapter;

	u8 *pallocated_buf;

	u8 *pbuf;

	void *priv_data;

	u16 buf_tag; /* 0: Normal xmitbuf, 1: extension xmitbuf, 2:cmd xmitbuf */
	u16 flags;
	u32 alloc_sz;

	u32  len;

	struct submit_ctx *sctx;

	/*struct tx_buf_desc *buf_desc;*/


};

struct xmit_frame {
	_list	list;

	struct pkt_attrib attrib;

	_pkt *pkt;

	int	frame_tag;

	_adapter *padapter;

	u8	*buf_addr;

	struct xmit_buf *pxmitbuf;

	u8 ack_report;

	u8 *alloc_addr; /* the actual address this xmitframe allocated */
	u8 ext_tag; /* 0:data, 1:mgmt */

};

struct tx_servq {
	_list	tx_pending;
	_queue	sta_pending;
	int qcnt;
};

struct sta_xmit_priv {
	_lock	lock;
	sint	option;
	sint	apsd_setting;	/* When bit mask is on, the associated edca queue supports APSD. */

	/* struct tx_servq blk_q[MAX_NUMBLKS]; */
	struct tx_servq	be_q;			/* priority == 0,3 */
	struct tx_servq	bk_q;			/* priority == 1,2 */
	struct tx_servq	vi_q;			/* priority == 4,5 */
	struct tx_servq	vo_q;			/* priority == 6,7 */
	_list	legacy_dz;
	_list  apsd;

	u16 txseq_tid[16];

	/* uint	sta_tx_bytes; */
	/* u64	sta_tx_pkts; */
	/* uint	sta_tx_fail; */

};

struct	hw_txqueue	{
	volatile sint	head;
	volatile sint	tail;
	volatile sint 	free_sz;	/* in units of 64 bytes */
	volatile sint      free_cmdsz;
	volatile sint	 txsz[8];
	uint	ff_hwaddr;
	uint	cmd_hwaddr;
	sint	ac_tag;
};

struct agg_pkt_info {
	u16 offset;
	u16 pkt_len;
};

enum cmdbuf_type {
	CMDBUF_BEACON = 0x00,
	CMDBUF_RSVD,
	CMDBUF_MAX
};

u8 rtw_get_hwseq_no(_adapter *padapter);

struct	xmit_priv	{

	_lock	lock;

	_sema	xmit_sema;
	/*_sema	terminate_xmitthread_sema;*/
	_completion xmitthread_comp;

	/* _queue	blk_strms[MAX_NUMBLKS]; */
	_queue	be_pending;
	_queue	bk_pending;
	_queue	vi_pending;
	_queue	vo_pending;
	_queue	bm_pending;

	/* _queue	legacy_dz_queue; */
	/* _queue	apsd_queue; */

	u8 *pallocated_frame_buf;
	u8 *pxmit_frame_buf;
	uint free_xmitframe_cnt;
	_queue	free_xmit_queue;

	/* uint mapping_addr; */
	/* uint pkt_sz; */

	u8 *xframe_ext_alloc_addr;
	u8 *xframe_ext;
	uint free_xframe_ext_cnt;
	_queue free_xframe_ext_queue;

	/* struct	hw_txqueue	be_txqueue; */
	/* struct	hw_txqueue	bk_txqueue; */
	/* struct	hw_txqueue	vi_txqueue; */
	/* struct	hw_txqueue	vo_txqueue; */
	/* struct	hw_txqueue	bmc_txqueue; */

	uint	frag_len;

	_adapter	*adapter;

	u8   vcs_setting;
	u8	vcs;
	u8	vcs_type;
	/* u16  rts_thresh; */

	u64	tx_bytes;
	u64	tx_pkts;
	u64	tx_drop;
	u64	last_tx_pkts;

	struct hw_xmit *hwxmits;
	u8	hwxmit_entry;

	u8	wmm_para_seq[4];/* sequence for wmm ac parameter strength from large to small. it's value is 0->vo, 1->vi, 2->be, 3->bk. */

	/* Tx */
	struct rtw_tx_ring	tx_ring[PCI_MAX_TX_QUEUE_COUNT];
	int	txringcount[PCI_MAX_TX_QUEUE_COUNT];
	u8 	beaconDMAing;		/* flag of indicating beacon is transmiting to HW by DMA */
	struct tasklet_struct xmit_tasklet;
	_queue free_xmitbuf_queue;
	_queue pending_xmitbuf_queue;
	u8 *pallocated_xmitbuf;
	u8 *pxmitbuf;
	uint free_xmitbuf_cnt;

	_queue free_xmit_extbuf_queue;
	u8 *pallocated_xmit_extbuf;
	u8 *pxmit_extbuf;
	uint free_xmit_extbuf_cnt;

	struct xmit_buf	pcmd_xmitbuf[CMDBUF_MAX];
	u8   hw_ssn_seq_no;/* mapping to REG_HW_SEQ 0,1,2,3 */
	u16	nqos_ssn;

	int	ack_tx;
	_mutex ack_tx_mutex;
	struct submit_ctx ack_tx_ops;
	u8 seq_no;

	_lock lock_sctx;

	u8 stop_req;
};

extern struct xmit_frame *__rtw_alloc_cmdxmitframe(struct xmit_priv *pxmitpriv,
		enum cmdbuf_type buf_type);
#define rtw_alloc_cmdxmitframe(p) __rtw_alloc_cmdxmitframe(p, CMDBUF_RSVD)
extern struct xmit_frame *__rtw_alloc_cmdxmitframe_8821ce(struct xmit_priv *pxmitpriv,
		enum cmdbuf_type buf_type);
#define rtw_alloc_bcnxmitframe(p) __rtw_alloc_cmdxmitframe_8821ce(p, CMDBUF_BEACON)

extern struct xmit_buf *rtw_alloc_xmitbuf_ext(struct xmit_priv *pxmitpriv);
extern s32 rtw_free_xmitbuf_ext(struct xmit_priv *pxmitpriv, struct xmit_buf *pxmitbuf);

extern struct xmit_buf *rtw_alloc_xmitbuf(struct xmit_priv *pxmitpriv);
extern s32 rtw_free_xmitbuf(struct xmit_priv *pxmitpriv, struct xmit_buf *pxmitbuf);

void rtw_count_tx_stats(_adapter *padapter, struct xmit_frame *pxmitframe, int sz);
extern void rtw_update_protection(_adapter *padapter, u8 *ie, uint ie_len);
extern s32 rtw_make_wlanhdr(_adapter *padapter, u8 *hdr, struct pkt_attrib *pattrib);
extern s32 rtw_put_snap(u8 *data, u16 h_proto);

extern struct xmit_frame *rtw_alloc_xmitframe(struct xmit_priv *pxmitpriv);
struct xmit_frame *rtw_alloc_xmitframe_ext(struct xmit_priv *pxmitpriv);
struct xmit_frame *rtw_alloc_xmitframe_once(struct xmit_priv *pxmitpriv);
extern s32 rtw_free_xmitframe(struct xmit_priv *pxmitpriv, struct xmit_frame *pxmitframe);
extern void rtw_free_xmitframe_queue(struct xmit_priv *pxmitpriv, _queue *pframequeue);
struct tx_servq *rtw_get_sta_pending(_adapter *padapter, struct sta_info *psta, sint up, u8 *ac);
extern s32 rtw_xmitframe_enqueue(_adapter *padapter, struct xmit_frame *pxmitframe);
extern struct xmit_frame *rtw_dequeue_xframe(struct xmit_priv *pxmitpriv, struct hw_xmit *phwxmit_i, sint entry);

extern s32 rtw_xmit_classifier(_adapter *padapter, struct xmit_frame *pxmitframe);
extern u32 rtw_calculate_wlan_pkt_size_by_attribue(struct pkt_attrib *pattrib);
#define rtw_wlan_pkt_size(f) rtw_calculate_wlan_pkt_size_by_attribue(&f->attrib)
extern s32 rtw_xmitframe_coalesce(_adapter *padapter, _pkt *pkt, struct xmit_frame *pxmitframe);
s32 _rtw_init_hw_txqueue(struct hw_txqueue *phw_txqueue, u8 ac_tag);
void _rtw_init_sta_xmit_priv(struct sta_xmit_priv *psta_xmitpriv);

s32 rtw_txframes_pending(_adapter *padapter);
s32 rtw_txframes_sta_ac_pending(_adapter *padapter, struct pkt_attrib *pattrib);
void rtw_init_hwxmits(struct hw_xmit *phwxmit, sint entry);

s32 _rtw_init_xmit_priv(struct xmit_priv *pxmitpriv, _adapter *padapter);
void _rtw_free_xmit_priv(struct xmit_priv *pxmitpriv);

void rtw_alloc_hwxmits(_adapter *padapter);
void rtw_free_hwxmits(_adapter *padapter);
s32 rtw_monitor_xmit_entry(struct sk_buff *skb, struct net_device *ndev);
s32 rtw_xmit(_adapter *padapter, _pkt **pkt);
bool xmitframe_hiq_filter(struct xmit_frame *xmitframe);
sint xmitframe_enqueue_for_sleeping_sta(_adapter *padapter, struct xmit_frame *pxmitframe);
void stop_sta_xmit(_adapter *padapter, struct sta_info *psta);
void wakeup_sta_to_xmit(_adapter *padapter, struct sta_info *psta);
void xmit_delivery_enabled_frames(_adapter *padapter, struct sta_info *psta);

u8 rtw_get_tx_bw_mode(_adapter *adapter, struct sta_info *sta);

void rtw_get_adapter_tx_rate_bmp_by_bw(_adapter *adapter, u8 bw, u16 *r_bmp_cck_ofdm, u32 *r_bmp_ht, u32 *r_bmp_vht);
void rtw_update_tx_rate_bmp(struct dvobj_priv *dvobj);
u16 rtw_get_tx_rate_bmp_cck_ofdm(struct dvobj_priv *dvobj);
u32 rtw_get_tx_rate_bmp_ht_by_bw(struct dvobj_priv *dvobj, u8 bw);
u32 rtw_get_tx_rate_bmp_vht_by_bw(struct dvobj_priv *dvobj, u8 bw);
u8 rtw_get_tx_bw_bmp_of_ht_rate(struct dvobj_priv *dvobj, u8 rate, u8 max_bw);
u8 rtw_get_tx_bw_bmp_of_vht_rate(struct dvobj_priv *dvobj, u8 rate, u8 max_bw);

u8 query_ra_short_GI(struct sta_info *psta, u8 bw);

u8	qos_acm(u8 acm_mask, u8 priority);



u32	rtw_get_ff_hwaddr(struct xmit_frame	*pxmitframe);

int rtw_ack_tx_wait(struct xmit_priv *pxmitpriv, u32 timeout_ms);
void rtw_ack_tx_done(struct xmit_priv *pxmitpriv, int status);

enum XMIT_BLOCK_REASON {
	XMIT_BLOCK_NONE = 0,
	XMIT_BLOCK_REDLMEM = BIT0, /*LPS-PG*/
	XMIT_BLOCK_SUSPEND = BIT1, /*WOW*/
	XMIT_BLOCK_MAX = 0xFF,
};
void rtw_init_xmit_block(_adapter *padapter);
void rtw_deinit_xmit_block(_adapter *padapter);

void rtw_set_xmit_block(_adapter *padapter, enum XMIT_BLOCK_REASON reason);
void rtw_clr_xmit_block(_adapter *padapter, enum XMIT_BLOCK_REASON reason);
bool rtw_is_xmit_blocked(_adapter *padapter);

/* include after declaring struct xmit_buf, in order to avoid warning */
#include <xmit_osdep.h>

#endif /* _RTL871X_XMIT_H_ */
