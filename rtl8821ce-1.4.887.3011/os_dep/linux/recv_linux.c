/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/
#define _RECV_OSDEP_C_

#include <drv_types.h>

int rtw_os_recvframe_duplicate_skb(_adapter *padapter, union recv_frame *pcloneframe, _pkt *pskb)
{
	int res = _SUCCESS;
	_pkt	*pkt_copy = NULL;

	if (pskb == NULL) {
		RTW_INFO("%s [WARN] skb == NULL, drop frag frame\n", __func__);
		return _FAIL;
	}
	pkt_copy = rtw_skb_copy(pskb);

	if (pkt_copy == NULL) {
		RTW_INFO("%s [WARN] rtw_skb_copy fail , drop frag frame\n", __func__);
		return _FAIL;
	}
	pkt_copy->dev = padapter->pnetdev;

	pcloneframe->u.hdr.pkt = pkt_copy;
	pcloneframe->u.hdr.rx_head = pkt_copy->head;
	pcloneframe->u.hdr.rx_data = pkt_copy->data;
	pcloneframe->u.hdr.rx_end = skb_end_pointer(pkt_copy);
	pcloneframe->u.hdr.rx_tail = skb_tail_pointer(pkt_copy);
	pcloneframe->u.hdr.len = pkt_copy->len;

	return res;
}

int rtw_os_alloc_recvframe(_adapter *padapter, union recv_frame *precvframe, u8 *pdata, _pkt *pskb)
{
	int res = _SUCCESS;
	u8	shift_sz = 0;
	u32	skb_len, alloc_sz;
	_pkt	*pkt_copy = NULL;
	struct rx_pkt_attrib *pattrib = &precvframe->u.hdr.attrib;

	if (pdata == NULL) {
		precvframe->u.hdr.pkt = NULL;
		res = _FAIL;
		return res;
	}

	/*	Modified by Albert 20101213 */
	/*	For 8 bytes IP header alignment. */
	shift_sz = pattrib->qos ? 6 : 0; /*	Qos data, wireless lan header length is 26 */

	skb_len = pattrib->pkt_len;

	/* for first fragment packet, driver need allocate 1536+drvinfo_sz+RXDESC_SIZE to defrag packet. */
	/* modify alloc_sz for recvive crc error packet by thomas 2011-06-02 */
	if ((pattrib->mfrag == 1) && (pattrib->frag_num == 0)) {
		/* alloc_sz = 1664;	 */ /* 1664 is 128 alignment. */
		alloc_sz = (skb_len <= 1650) ? 1664 : (skb_len + 14);
	} else {
		alloc_sz = skb_len;
		/*	6 is for IP header 8 bytes alignment in QoS packet case. */
		/*	8 is for skb->data 4 bytes alignment. */
		alloc_sz += 14;
	}

	pkt_copy = rtw_skb_alloc(alloc_sz);

	if (pkt_copy) {
		pkt_copy->dev = padapter->pnetdev;
		pkt_copy->len = skb_len;
		precvframe->u.hdr.pkt = pkt_copy;
		precvframe->u.hdr.rx_head = pkt_copy->head;
		precvframe->u.hdr.rx_end = pkt_copy->data + alloc_sz;
		skb_reserve(pkt_copy, 8 - ((SIZE_PTR)(pkt_copy->data) & 7));  /* force pkt_copy->data at 8-byte alignment address */
		skb_reserve(pkt_copy, shift_sz);/* force ip_hdr at 8-byte alignment address according to shift_sz. */
		_rtw_memcpy(pkt_copy->data, pdata, skb_len);
		precvframe->u.hdr.rx_data = precvframe->u.hdr.rx_tail = pkt_copy->data;
	} else {

		if ((pattrib->mfrag == 1) && (pattrib->frag_num == 0)) {
			RTW_INFO("%s: alloc_skb fail , drop frag frame\n", __FUNCTION__);
			/* rtw_free_recvframe(precvframe, pfree_recv_queue); */
			res = _FAIL;
			goto exit_rtw_os_recv_resource_alloc;
		}

		if (pskb == NULL) {
			res = _FAIL;
			goto exit_rtw_os_recv_resource_alloc;
		}

		precvframe->u.hdr.pkt = rtw_skb_clone(pskb);
		if (precvframe->u.hdr.pkt) {
			precvframe->u.hdr.pkt->dev = padapter->pnetdev;
			precvframe->u.hdr.rx_head = precvframe->u.hdr.rx_data = precvframe->u.hdr.rx_tail = pdata;
			precvframe->u.hdr.rx_end =  pdata + alloc_sz;
		} else {
			RTW_INFO("%s: rtw_skb_clone fail\n", __FUNCTION__);
			/* rtw_free_recvframe(precvframe, pfree_recv_queue); */
			/*exit_rtw_os_recv_resource_alloc;*/
			res = _FAIL;
		}
	}

exit_rtw_os_recv_resource_alloc:

	return res;

}

void rtw_os_free_recvframe(union recv_frame *precvframe)
{
	if (precvframe->u.hdr.pkt) {
		rtw_skb_free(precvframe->u.hdr.pkt);/* free skb by driver */

		precvframe->u.hdr.pkt = NULL;
	}
}

/* init os related resource in struct recv_priv */
int rtw_os_recv_resource_init(struct recv_priv *precvpriv, _adapter *padapter)
{
	int	res = _SUCCESS;

	skb_queue_head_init(&precvpriv->rx_napi_skb_queue);

	return res;
}

/* alloc os related resource in union recv_frame */
int rtw_os_recv_resource_alloc(_adapter *padapter, union recv_frame *precvframe)
{
	int	res = _SUCCESS;

	precvframe->u.hdr.pkt_newalloc = precvframe->u.hdr.pkt = NULL;

	return res;
}

/* free os related resource in union recv_frame */
void rtw_os_recv_resource_free(struct recv_priv *precvpriv)
{
	sint i;
	union recv_frame *precvframe;
	precvframe = (union recv_frame *) precvpriv->precv_frame_buf;

	if (skb_queue_len(&precvpriv->rx_napi_skb_queue))
		RTW_WARN("rx_napi_skb_queue not empty\n");
	rtw_skb_queue_purge(&precvpriv->rx_napi_skb_queue);

	for (i = 0; i < NR_RECVFRAME; i++) {
		if (precvframe->u.hdr.pkt) {
			rtw_skb_free(precvframe->u.hdr.pkt);/* free skb by driver */
			precvframe->u.hdr.pkt = NULL;
		}
		precvframe++;
	}
}

/* alloc os related resource in struct recv_buf */
int rtw_os_recvbuf_resource_alloc(_adapter *padapter, struct recv_buf *precvbuf)
{
	int res = _SUCCESS;

	return res;
}

/* free os related resource in struct recv_buf */
int rtw_os_recvbuf_resource_free(_adapter *padapter, struct recv_buf *precvbuf)
{
	int ret = _SUCCESS;

	if (precvbuf->pskb) {
			rtw_skb_free(precvbuf->pskb);
	}
	return ret;

}

_pkt *rtw_os_alloc_msdu_pkt(union recv_frame *prframe, u16 nSubframe_Length, u8 *pdata)
{
	u16	eth_type;
	u8	*data_ptr;
	_pkt *sub_skb;
	struct rx_pkt_attrib *pattrib;

	pattrib = &prframe->u.hdr.attrib;

	sub_skb = rtw_skb_alloc(nSubframe_Length + 12);
	if (sub_skb) {
		skb_reserve(sub_skb, 12);
		data_ptr = (u8 *)skb_put(sub_skb, nSubframe_Length);
		_rtw_memcpy(data_ptr, (pdata + ETH_HLEN), nSubframe_Length);
	} else
	{
		sub_skb = rtw_skb_clone(prframe->u.hdr.pkt);
		if (sub_skb) {
			sub_skb->data = pdata + ETH_HLEN;
			sub_skb->len = nSubframe_Length;
			skb_set_tail_pointer(sub_skb, nSubframe_Length);
		} else {
			RTW_INFO("%s(): rtw_skb_clone() Fail!!!\n", __FUNCTION__);
			return NULL;
		}
	}

	eth_type = RTW_GET_BE16(&sub_skb->data[6]);

	if (sub_skb->len >= 8 &&
	    ((_rtw_memcmp(sub_skb->data, rtw_rfc1042_header, SNAP_SIZE) &&
	      eth_type != ETH_P_AARP && eth_type != ETH_P_IPX) ||
	     _rtw_memcmp(sub_skb->data, rtw_bridge_tunnel_header, SNAP_SIZE))) {
		/* remove RFC1042 or Bridge-Tunnel encapsulation and replace EtherType */
		skb_pull(sub_skb, SNAP_SIZE);
		_rtw_memcpy(skb_push(sub_skb, ETH_ALEN), pattrib->src, ETH_ALEN);
		_rtw_memcpy(skb_push(sub_skb, ETH_ALEN), pattrib->dst, ETH_ALEN);
	} else {
		u16 len;
		/* Leave Ethernet header part of hdr and full payload */
		len = htons(sub_skb->len);
		_rtw_memcpy(skb_push(sub_skb, 2), &len, 2);
		_rtw_memcpy(skb_push(sub_skb, ETH_ALEN), pattrib->src, ETH_ALEN);
		_rtw_memcpy(skb_push(sub_skb, ETH_ALEN), pattrib->dst, ETH_ALEN);
	}

	return sub_skb;
}

static int napi_recv(_adapter *padapter, int budget)
{
	_pkt *pskb;
	struct recv_priv *precvpriv = &padapter->recvpriv;
	int work_done = 0;
	struct registry_priv *pregistrypriv = &padapter->registrypriv;
	u8 rx_ok;

	while ((work_done < budget) &&
	       (!skb_queue_empty(&precvpriv->rx_napi_skb_queue))) {
		pskb = skb_dequeue(&precvpriv->rx_napi_skb_queue);
		if (!pskb)
			break;

		rx_ok = _FALSE;

		if (pregistrypriv->en_gro) {
			if (rtw_napi_gro_receive(&padapter->napi, pskb) != GRO_DROP)
				rx_ok = _TRUE;
			goto next;
		}

		if (rtw_netif_receive_skb(padapter->pnetdev, pskb) == NET_RX_SUCCESS)
			rx_ok = _TRUE;

next:
		if (rx_ok == _TRUE) {
			work_done++;
			DBG_COUNTER(padapter->rx_logs.os_netif_ok);
		} else {
			DBG_COUNTER(padapter->rx_logs.os_netif_err);
		}
	}

	return work_done;
}

int rtw_recv_napi_poll(struct napi_struct *napi, int budget)
{
	_adapter *padapter = container_of(napi, _adapter, napi);
	int work_done = 0;
	struct recv_priv *precvpriv = &padapter->recvpriv;

	work_done = napi_recv(padapter, budget);
	if (work_done < budget) {
		napi_complete(napi);
		if (!skb_queue_empty(&precvpriv->rx_napi_skb_queue))
			napi_schedule(napi);
	}

	return work_done;
}


void rtw_os_recv_indicate_pkt(_adapter *padapter, _pkt *pkt, struct rx_pkt_attrib *pattrib)
{
	struct mlme_priv *pmlmepriv = &padapter->mlmepriv;
	struct recv_priv *precvpriv = &(padapter->recvpriv);
	struct registry_priv	*pregistrypriv = &padapter->registrypriv;
	void *br_port = NULL;
	int ret;

	/* Indicat the packets to upper layer */
	if (pkt) {
		if (check_fwstate(pmlmepriv, WIFI_AP_STATE) == _TRUE) {
			_pkt *pskb2 = NULL;
			struct sta_info *psta = NULL;
			struct sta_priv *pstapriv = &padapter->stapriv;
			int bmcast = IS_MCAST(pattrib->dst);

			/* RTW_INFO("bmcast=%d\n", bmcast); */

			if (_rtw_memcmp(pattrib->dst, adapter_mac_addr(padapter), ETH_ALEN) == _FALSE) {
				/* RTW_INFO("not ap psta=%p, addr=%pM\n", psta, pattrib->dst); */

				if (bmcast) {
					psta = rtw_get_bcmc_stainfo(padapter);
					pskb2 = rtw_skb_clone(pkt);
				} else
					psta = rtw_get_stainfo(pstapriv, pattrib->dst);

				if (psta) {
					struct net_device *pnetdev = (struct net_device *)padapter->pnetdev;

					/* RTW_INFO("directly forwarding to the rtw_xmit_entry\n"); */

					/* skb->ip_summed = CHECKSUM_NONE; */
					pkt->dev = pnetdev;
					skb_set_queue_mapping(pkt, rtw_recv_select_queue(pkt));

					_rtw_xmit_entry(pkt, pnetdev);

					if (bmcast && (pskb2 != NULL)) {
						pkt = pskb2;
						DBG_COUNTER(padapter->rx_logs.os_indicate_ap_mcast);
					} else {
						DBG_COUNTER(padapter->rx_logs.os_indicate_ap_forward);
						return;
					}
				}
			} else { /* to APself */
				/* RTW_INFO("to APSelf\n"); */
				DBG_COUNTER(padapter->rx_logs.os_indicate_ap_self);
			}
		}

		/* Insert NAT2.5 RX here! */
		rcu_read_lock();
		br_port = rcu_dereference(padapter->pnetdev->rx_handler_data);
		rcu_read_unlock();

		if (br_port && (check_fwstate(pmlmepriv, WIFI_STATION_STATE | WIFI_ADHOC_STATE) == _TRUE)) {
			int nat25_handle_frame(_adapter *priv, struct sk_buff *skb);
			if (nat25_handle_frame(padapter, pkt) == -1) {
			}
		}
		if (precvpriv->sink_udpport > 0)
			rtw_sink_rtp_seq_dbg(padapter, pkt);
		/* After eth_type_trans process , pkt->data pointer will move from ethrnet header to ip header */
		pkt->protocol = eth_type_trans(pkt, padapter->pnetdev);
		pkt->dev = padapter->pnetdev;

		pkt->ip_summed = CHECKSUM_NONE;

		if (pregistrypriv->en_napi) {
			skb_queue_tail(&precvpriv->rx_napi_skb_queue, pkt);
			napi_schedule(&padapter->napi);
			return;
		}

		ret = rtw_netif_rx(padapter->pnetdev, pkt);
		if (ret == NET_RX_SUCCESS)
			DBG_COUNTER(padapter->rx_logs.os_netif_ok);
		else
			DBG_COUNTER(padapter->rx_logs.os_netif_err);
	}
}

void rtw_handle_tkip_mic_err(_adapter *padapter, struct sta_info *sta, u8 bgroup)
{
	union iwreq_data wrqu;
	struct iw_michaelmicfailure    ev;
	struct security_priv	*psecuritypriv = &padapter->securitypriv;
	u32 cur_time = 0;

	if (psecuritypriv->last_mic_err_time == 0)
		psecuritypriv->last_mic_err_time = rtw_get_current_time();
	else {
		cur_time = rtw_get_current_time();

		if (cur_time - psecuritypriv->last_mic_err_time < 60 * HZ) {
			psecuritypriv->btkip_countermeasure = _TRUE;
			psecuritypriv->last_mic_err_time = 0;
			psecuritypriv->btkip_countermeasure_time = cur_time;
		} else
			psecuritypriv->last_mic_err_time = rtw_get_current_time();
	}


	_rtw_memset(&ev, 0x00, sizeof(ev));
	if (bgroup)
		ev.flags |= IW_MICFAILURE_GROUP;
	else
		ev.flags |= IW_MICFAILURE_PAIRWISE;

	ev.src_addr.sa_family = ARPHRD_ETHER;
	_rtw_memcpy(ev.src_addr.sa_data, sta->hwaddr, ETH_ALEN);

	_rtw_memset(&wrqu, 0x00, sizeof(wrqu));
	wrqu.data.length = sizeof(ev);

	wireless_send_event(padapter->pnetdev, IWEVMICHAELMICFAILURE, &wrqu, (char *) &ev);
}

void rtw_hostapd_mlme_rx(_adapter *padapter, union recv_frame *precv_frame)
{
}


int rtw_recv_monitor(_adapter *padapter, union recv_frame *precv_frame)
{
	int ret = _FAIL;
	struct recv_priv *precvpriv;
	_queue	*pfree_recv_queue;
	_pkt *skb;
	struct rx_pkt_attrib *pattrib;

	if (NULL == precv_frame)
		goto _recv_drop;

	pattrib = &precv_frame->u.hdr.attrib;
	precvpriv = &(padapter->recvpriv);
	pfree_recv_queue = &(precvpriv->free_recv_queue);

	skb = precv_frame->u.hdr.pkt;
	if (skb == NULL) {
		RTW_INFO("%s :skb==NULL something wrong!!!!\n", __func__);
		goto _recv_drop;
	}

	skb->data = precv_frame->u.hdr.rx_data;
	skb_set_tail_pointer(skb, precv_frame->u.hdr.len);
	skb->len = precv_frame->u.hdr.len;
	skb->ip_summed = CHECKSUM_NONE;
	skb->pkt_type = PACKET_OTHERHOST;
	skb->protocol = htons(0x0019); /* ETH_P_80211_RAW */

	rtw_netif_rx(padapter->pnetdev, skb);

	/* pointers to NULL before rtw_free_recvframe() */
	precv_frame->u.hdr.pkt = NULL;

	ret = _SUCCESS;

_recv_drop:

	/* enqueue back to free_recv_queue */
	if (precv_frame)
		rtw_free_recvframe(precv_frame, pfree_recv_queue);

	return ret;

}

int rtw_recv_indicatepkt(_adapter *padapter, union recv_frame *precv_frame)
{
	struct recv_priv *precvpriv;
	_queue	*pfree_recv_queue;
	_pkt *skb;
	struct rx_pkt_attrib *pattrib;

	if (NULL == precv_frame)
		goto _recv_indicatepkt_drop;

	DBG_COUNTER(padapter->rx_logs.os_indicate);
	pattrib = &precv_frame->u.hdr.attrib;
	precvpriv = &(padapter->recvpriv);
	pfree_recv_queue = &(precvpriv->free_recv_queue);


	skb = precv_frame->u.hdr.pkt;
	if (skb == NULL) {
		goto _recv_indicatepkt_drop;
	}

	skb->data = precv_frame->u.hdr.rx_data;

	skb_set_tail_pointer(skb, precv_frame->u.hdr.len);

	skb->len = precv_frame->u.hdr.len;

	if (pattrib->eth_type == 0x888e)
		RTW_PRINT("recv eapol packet\n");


	/* TODO: move to core */
	{
		_pkt *pkt = skb;
		struct ethhdr *etherhdr = (struct ethhdr *)pkt->data;
		struct sta_info *sta = precv_frame->u.hdr.psta;

		if (!sta)
			goto bypass_session_tracker;

		if (ntohs(etherhdr->h_proto) == ETH_P_IP) {
			u8 *ip = pkt->data + 14;

			if (GET_IPV4_PROTOCOL(ip) == 0x06  /* TCP */
			    && rtw_st_ctl_chk_reg_s_proto(&sta->st_ctl, 0x06) == _TRUE
			   ) {
				u8 *tcp = ip + GET_IPV4_IHL(ip) * 4;

				if (rtw_st_ctl_chk_reg_rule(&sta->st_ctl, padapter, IPV4_DST(ip), TCP_DST(tcp), IPV4_SRC(ip), TCP_SRC(tcp)) == _TRUE) {
					if (GET_TCP_SYN(tcp) && GET_TCP_ACK(tcp)) {
						session_tracker_add_cmd(padapter, sta
							, IPV4_DST(ip), TCP_DST(tcp)
							, IPV4_SRC(ip), TCP_SRC(tcp));
						if (DBG_SESSION_TRACKER)
							RTW_INFO(FUNC_ADPT_FMT" local:"IP_FMT":"PORT_FMT", remote:"IP_FMT":"PORT_FMT" SYN-ACK\n"
								, FUNC_ADPT_ARG(padapter)
								, IP_ARG(IPV4_DST(ip)), PORT_ARG(TCP_DST(tcp))
								, IP_ARG(IPV4_SRC(ip)), PORT_ARG(TCP_SRC(tcp)));
					}
					if (GET_TCP_FIN(tcp)) {
						session_tracker_del_cmd(padapter, sta
							, IPV4_DST(ip), TCP_DST(tcp)
							, IPV4_SRC(ip), TCP_SRC(tcp));
						if (DBG_SESSION_TRACKER)
							RTW_INFO(FUNC_ADPT_FMT" local:"IP_FMT":"PORT_FMT", remote:"IP_FMT":"PORT_FMT" FIN\n"
								, FUNC_ADPT_ARG(padapter)
								, IP_ARG(IPV4_DST(ip)), PORT_ARG(TCP_DST(tcp))
								, IP_ARG(IPV4_SRC(ip)), PORT_ARG(TCP_SRC(tcp)));
					}
				}

			}
		}
bypass_session_tracker:
		;
	}

	rtw_os_recv_indicate_pkt(padapter, skb, pattrib);

	precv_frame->u.hdr.pkt = NULL; /* pointers to NULL before rtw_free_recvframe() */

	rtw_free_recvframe(precv_frame, pfree_recv_queue);

	return _SUCCESS;

_recv_indicatepkt_drop:

	/* enqueue back to free_recv_queue */
	if (precv_frame)
		rtw_free_recvframe(precv_frame, pfree_recv_queue);

	DBG_COUNTER(padapter->rx_logs.os_indicate_err);

	return _FAIL;

}
