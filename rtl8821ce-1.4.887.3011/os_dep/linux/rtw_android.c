/* SPDX-License-Identifier: GPL-2.0 */
/******************************************************************************
 *
 * Copyright(c) 2007 - 2011 Realtek Corporation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of version 2 of the GNU General Public License as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110, USA
 *
 *
 ******************************************************************************/

#include <drv_types.h>

#define strnicmp	strncasecmp

#include "rtw_version.h"

extern void macstr2num(u8 *dst, u8 *src);

const char *android_wifi_cmd_str[ANDROID_WIFI_CMD_MAX] = {
	"START",
	"STOP",
	"SCAN-ACTIVE",
	"SCAN-PASSIVE",
	"RSSI",
	"LINKSPEED",
	"RXFILTER-START",
	"RXFILTER-STOP",
	"RXFILTER-ADD",
	"RXFILTER-REMOVE",
	"BTCOEXSCAN-START",
	"BTCOEXSCAN-STOP",
	"BTCOEXMODE",
	"SETSUSPENDOPT",
	"P2P_DEV_ADDR",
	"SETFWPATH",
	"SETBAND",
	"GETBAND",
	"COUNTRY",
	"P2P_SET_NOA",
	"P2P_GET_NOA",
	"P2P_SET_PS",
	"SET_AP_WPS_P2P_IE",

	"MIRACAST",


	"MACADDR",

	"BLOCK_SCAN",
	"BLOCK",
	"WFD-ENABLE",
	"WFD-DISABLE",
	"WFD-SET-TCPPORT",
	"WFD-SET-MAXTPUT",
	"WFD-SET-DEVTYPE",
	"SET_DTIM",
	"HOSTAPD_SET_MACADDR_ACL",
	"HOSTAPD_ACL_ADD_STA",
	"HOSTAPD_ACL_REMOVE_STA",
/*	Private command for	P2P disable*/
	"P2P_DISABLE",
	"DRIVER_VERSION"
};


typedef struct android_wifi_priv_cmd {
	char *buf;
	int used_len;
	int total_len;
} android_wifi_priv_cmd;

/**
 * Local (static) functions and variables
 */

/* Initialize g_wifi_on to 1 so dhd_bus_start will be called for the first
 * time (only) in dhd_open, subsequential wifi on will be handled by
 * wl_android_wifi_on
 */
static int g_wifi_on = _TRUE;

unsigned int oob_irq = 0;
unsigned int oob_gpio = 0;


int rtw_android_cmdstr_to_num(char *cmdstr)
{
	int cmd_num;
	for (cmd_num = 0 ; cmd_num < ANDROID_WIFI_CMD_MAX; cmd_num++)
		if (0 == strnicmp(cmdstr , android_wifi_cmd_str[cmd_num], strlen(android_wifi_cmd_str[cmd_num])))
			break;

	return cmd_num;
}

int rtw_android_get_rssi(struct net_device *net, char *command, int total_len)
{
	_adapter *padapter = (_adapter *)rtw_netdev_priv(net);
	struct	mlme_priv	*pmlmepriv = &(padapter->mlmepriv);
	struct	wlan_network	*pcur_network = &pmlmepriv->cur_network;
	int bytes_written = 0;

	if (check_fwstate(pmlmepriv, _FW_LINKED) == _TRUE) {
		bytes_written += snprintf(&command[bytes_written], total_len, "%s rssi %d",
			pcur_network->network.Ssid.Ssid, padapter->recvpriv.rssi);
	}

	return bytes_written;
}

int rtw_android_get_link_speed(struct net_device *net, char *command, int total_len)
{
	_adapter *padapter = (_adapter *)rtw_netdev_priv(net);
	int bytes_written = 0;
	u16 link_speed = 0;

	link_speed = rtw_get_cur_max_rate(padapter) / 10;
	bytes_written = snprintf(command, total_len, "LinkSpeed %d", link_speed);

	return bytes_written;
}

int rtw_android_get_macaddr(struct net_device *net, char *command, int total_len)
{
	int bytes_written = 0;

	bytes_written = snprintf(command, total_len, "Macaddr = "MAC_FMT, MAC_ARG(net->dev_addr));
	return bytes_written;
}

int rtw_android_set_country(struct net_device *net, char *command, int total_len)
{
	_adapter *adapter = (_adapter *)rtw_netdev_priv(net);
	char *country_code = command + strlen(android_wifi_cmd_str[ANDROID_WIFI_CMD_COUNTRY]) + 1;
	int ret = _FAIL;

	ret = rtw_set_country(adapter, country_code);

	return (ret == _SUCCESS) ? 0 : -1;
}

int rtw_android_get_p2p_dev_addr(struct net_device *net, char *command, int total_len)
{
	int bytes_written = 0;

	/* We use the same address as our HW MAC address */
	_rtw_memcpy(command, net->dev_addr, ETH_ALEN);

	bytes_written = ETH_ALEN;
	return bytes_written;
}

int rtw_android_setband(struct net_device *net, char *command, int total_len)
{
	_adapter *adapter = (_adapter *)rtw_netdev_priv(net);
	char *arg = command + strlen(android_wifi_cmd_str[ANDROID_WIFI_CMD_SETBAND]) + 1;
	u32 band = WIFI_FREQUENCY_BAND_AUTO;
	int ret = _FAIL;

	if (sscanf(arg, "%u", &band) >= 1)
		ret = rtw_set_band(adapter, band);

	return (ret == _SUCCESS) ? 0 : -1;
}

int rtw_android_getband(struct net_device *net, char *command, int total_len)
{
	_adapter *adapter = (_adapter *)rtw_netdev_priv(net);
	int bytes_written = 0;

	bytes_written = snprintf(command, total_len, "%u", adapter->setband);

	return bytes_written;
}

int rtw_android_set_miracast_mode(struct net_device *net, char *command, int total_len)
{
	_adapter *adapter = (_adapter *)rtw_netdev_priv(net);
	struct wifi_display_info *wfd_info = &adapter->wfd_info;
	char *arg = command + strlen(android_wifi_cmd_str[ANDROID_WIFI_CMD_MIRACAST]) + 1;
	u8 mode;
	int num;
	int ret = _FAIL;

	num = sscanf(arg, "%hhu", &mode);

	if (num < 1)
		goto exit;

	switch (mode) {
	case 1: /* soruce */
		mode = MIRACAST_SOURCE;
		break;
	case 2: /* sink */
		mode = MIRACAST_SINK;
		break;
	case 0: /* disabled */
	default:
		mode = MIRACAST_DISABLED;
		break;
	}
	wfd_info->stack_wfd_mode = mode;
	RTW_INFO("stack miracast mode: %s\n", get_miracast_mode_str(wfd_info->stack_wfd_mode));

	ret = _SUCCESS;

exit:
	return (ret == _SUCCESS) ? 0 : -1;
}

int get_int_from_command(char *pcmd)
{
	int i = 0;

	for (i = 0; i < strlen(pcmd); i++) {
		if (pcmd[i] == '=') {
			/*	Skip the '=' and space characters. */
			i += 2;
			break;
		}
	}
	return rtw_atoi(pcmd + i) ;
}


int rtw_android_priv_cmd(struct net_device *net, struct ifreq *ifr, int cmd)
{
	int ret = 0;
	char *command = NULL;
	int cmd_num;
	int bytes_written = 0;
	android_wifi_priv_cmd priv_cmd;
	_adapter	*padapter = (_adapter *) rtw_netdev_priv(net);
	struct wifi_display_info		*pwfd_info;

	rtw_lock_suspend();

	if (!ifr->ifr_data) {
		ret = -EINVAL;
		goto exit;
	}
	if (padapter->registrypriv.mp_mode == 1) {
		ret = -EINVAL;
		goto exit;
	}
		if (copy_from_user(&priv_cmd, ifr->ifr_data, sizeof(android_wifi_priv_cmd))) {
			ret = -EFAULT;
			goto exit;
		}
	if (padapter->registrypriv.mp_mode == 1) {
		ret = -EFAULT;
		goto exit;
	}
	/*RTW_INFO("%s priv_cmd.buf=%p priv_cmd.total_len=%d  priv_cmd.used_len=%d\n",__func__,priv_cmd.buf,priv_cmd.total_len,priv_cmd.used_len);*/
	command = rtw_zmalloc(priv_cmd.total_len);
	if (!command) {
		RTW_INFO("%s: failed to allocate memory\n", __FUNCTION__);
		ret = -ENOMEM;
		goto exit;
	}

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5, 0, 0))
	if (!access_ok(priv_cmd.buf, priv_cmd.total_len)) {
#else
	if (!access_ok(VERIFY_READ, priv_cmd.buf, priv_cmd.total_len)) {
#endif
		RTW_INFO("%s: failed to access memory\n", __FUNCTION__);
		ret = -EFAULT;
		goto exit;
	}
	if (copy_from_user(command, (void *)priv_cmd.buf, priv_cmd.total_len)) {
		ret = -EFAULT;
		goto exit;
	}

	RTW_INFO("%s: Android private cmd \"%s\" on %s\n"
		 , __FUNCTION__, command, ifr->ifr_name);

	cmd_num = rtw_android_cmdstr_to_num(command);

	switch (cmd_num) {
	case ANDROID_WIFI_CMD_START:
		/* bytes_written = wl_android_wifi_on(net); */
		goto response;
	case ANDROID_WIFI_CMD_SETFWPATH:
		goto response;
	}

	if (!g_wifi_on) {
		RTW_INFO("%s: Ignore private cmd \"%s\" - iface %s is down\n"
			 , __FUNCTION__, command, ifr->ifr_name);
		ret = 0;
		goto exit;
	}

	if (!hal_chk_wl_func(padapter, WL_FUNC_MIRACAST)) {
		switch (cmd_num) {
		case ANDROID_WIFI_CMD_WFD_ENABLE:
		case ANDROID_WIFI_CMD_WFD_DISABLE:
		case ANDROID_WIFI_CMD_WFD_SET_TCPPORT:
		case ANDROID_WIFI_CMD_WFD_SET_MAX_TPUT:
		case ANDROID_WIFI_CMD_WFD_SET_DEVTYPE:
			goto response;
		}
	}

	switch (cmd_num) {
	case ANDROID_WIFI_CMD_SCAN_ACTIVE:
	case ANDROID_WIFI_CMD_SCAN_PASSIVE:
	case ANDROID_WIFI_CMD_STOP:
	case ANDROID_WIFI_CMD_BLOCK_SCAN:
	case ANDROID_WIFI_CMD_BLOCK:
	case ANDROID_WIFI_CMD_RXFILTER_START:
	case ANDROID_WIFI_CMD_RXFILTER_STOP:
	case ANDROID_WIFI_CMD_RXFILTER_ADD:
	case ANDROID_WIFI_CMD_RXFILTER_REMOVE:
	case ANDROID_WIFI_CMD_BTCOEXSCAN_START:
	case ANDROID_WIFI_CMD_BTCOEXSCAN_STOP:
	case ANDROID_WIFI_CMD_BTCOEXMODE:
	case ANDROID_WIFI_CMD_SETSUSPENDOPT:
	case ANDROID_WIFI_CMD_SETBAND:
	case ANDROID_WIFI_CMD_P2P_SET_NOA:
	case ANDROID_WIFI_CMD_P2P_GET_NOA:
	case ANDROID_WIFI_CMD_P2P_SET_PS:
	case ANDROID_WIFI_CMD_WFD_SET_MAX_TPUT:
		break;

	case ANDROID_WIFI_CMD_RSSI:
		bytes_written = rtw_android_get_rssi(net, command, priv_cmd.total_len);
		break;
	case ANDROID_WIFI_CMD_LINKSPEED:
		bytes_written = rtw_android_get_link_speed(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_MACADDR:
		bytes_written = rtw_android_get_macaddr(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_GETBAND:
		bytes_written = rtw_android_getband(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_COUNTRY:
		bytes_written = rtw_android_set_country(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_P2P_DEV_ADDR:
		bytes_written = rtw_android_get_p2p_dev_addr(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_MIRACAST:
		bytes_written = rtw_android_set_miracast_mode(net, command, priv_cmd.total_len);
		break;

	case ANDROID_WIFI_CMD_WFD_ENABLE: {
		/*	Commented by Albert 2012/07/24 */
		/*	We can enable the WFD function by using the following command: */
		/*	wpa_cli driver wfd-enable */

		if (padapter->wdinfo.driver_interface == DRIVER_CFG80211)
			rtw_wfd_enable(padapter, 1);
		break;
	}

	case ANDROID_WIFI_CMD_WFD_DISABLE: {
		/*	Commented by Albert 2012/07/24 */
		/*	We can disable the WFD function by using the following command: */
		/*	wpa_cli driver wfd-disable */

		if (padapter->wdinfo.driver_interface == DRIVER_CFG80211)
			rtw_wfd_enable(padapter, 0);
		break;
	}
	case ANDROID_WIFI_CMD_WFD_SET_TCPPORT: {
		/*	Commented by Albert 2012/07/24 */
		/*	We can set the tcp port number by using the following command: */
		/*	wpa_cli driver wfd-set-tcpport = 554 */

		if (padapter->wdinfo.driver_interface == DRIVER_CFG80211)
			rtw_wfd_set_ctrl_port(padapter, (u16)get_int_from_command(priv_cmd.buf));
		break;
	}
	case ANDROID_WIFI_CMD_WFD_SET_DEVTYPE: {
		/*	Commented by Albert 2012/08/28 */
		/*	Specify the WFD device type ( WFD source/primary sink ) */

		pwfd_info = &padapter->wfd_info;
		if (padapter->wdinfo.driver_interface == DRIVER_CFG80211) {
			pwfd_info->wfd_device_type = (u8) get_int_from_command(priv_cmd.buf);
			pwfd_info->wfd_device_type &= WFD_DEVINFO_DUAL;
		}
		break;
	}
	case ANDROID_WIFI_CMD_CHANGE_DTIM: {
		u8 dtim;
		u8 *ptr = (u8 *) &priv_cmd.buf;

		ptr += 9;/* string command length of  "SET_DTIM"; */

		dtim = rtw_atoi(ptr);

		RTW_INFO("DTIM=%d\n", dtim);

		rtw_lps_change_dtim_cmd(padapter, dtim);
	}
	break;

	case ANDROID_WIFI_CMD_HOSTAPD_SET_MACADDR_ACL: {
		rtw_set_macaddr_acl(padapter, get_int_from_command(command));
		break;
	}
	case ANDROID_WIFI_CMD_HOSTAPD_ACL_ADD_STA: {
		u8 addr[ETH_ALEN] = {0x00};
		macstr2num(addr, command + strlen("HOSTAPD_ACL_ADD_STA") + 3);	/* 3 is space bar + "=" + space bar these 3 chars */
		rtw_acl_add_sta(padapter, addr);
		break;
	}
	case ANDROID_WIFI_CMD_HOSTAPD_ACL_REMOVE_STA: {
		u8 addr[ETH_ALEN] = {0x00};
		macstr2num(addr, command + strlen("HOSTAPD_ACL_REMOVE_STA") + 3);	/* 3 is space bar + "=" + space bar these 3 chars */
		rtw_acl_remove_sta(padapter, addr);
		break;
	}
	case ANDROID_WIFI_CMD_P2P_DISABLE: {
		rtw_p2p_enable(padapter, P2P_ROLE_DISABLE);
		break;
	}
	case ANDROID_WIFI_CMD_DRIVERVERSION: {
		bytes_written = strlen(DRIVERVERSION);
		snprintf(command, bytes_written + 1, DRIVERVERSION);
		break;
	}
	default:
		RTW_INFO("Unknown PRIVATE command %s - ignored\n", command);
		snprintf(command, 3, "OK");
		bytes_written = strlen("OK");
	}

response:
	if (bytes_written >= 0) {
		if ((bytes_written == 0) && (priv_cmd.total_len > 0))
			command[0] = '\0';
		if (bytes_written >= priv_cmd.total_len) {
			RTW_INFO("%s: bytes_written = %d\n", __FUNCTION__, bytes_written);
			bytes_written = priv_cmd.total_len;
		} else
			bytes_written++;
		priv_cmd.used_len = bytes_written;
		if (copy_to_user((void *)priv_cmd.buf, command, bytes_written)) {
			RTW_INFO("%s: failed to copy data to user buffer\n", __FUNCTION__);
			ret = -EFAULT;
		}
	} else
		ret = bytes_written;

exit:
	rtw_unlock_suspend();
	if (command)
		rtw_mfree(command, priv_cmd.total_len);

	return ret;
}

/**
 * Functions for Android WiFi card detection
 */
